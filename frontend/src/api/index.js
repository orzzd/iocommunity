export const SEND_POST_URL = 'https://io.se.jisuanke.com/IO/sendPostCtrl'
export const SEND_COMMENT_URL = 'https://io.se.jisuanke.com/IO/sendCommentCtrl'
export const GET_COMMENT_URL = 'https://io.se.jisuanke.com/IO/getCommentsCtrl'
export const OPENID_URL = 'https://io.se.jisuanke.com/IO/LoginCtrl'
export const APPID = 'wxe7b33b3ce8591707'
export const SECRET = '1d5da629d07cd0f3dc8b9b7f825107f4'
export const INSERT_USER_URL = 'https://io.se.jisuanke.com/IO/InsertNewUserCtrl'
export const GET_POST_URL = 'https://io.se.jisuanke.com/IO/getPostsCtrl'
export const GET_USER_INFO_URL = 'https://io.se.jisuanke.com/IO/GetUserAllInfoCtrl'
export const GET_USER_ALL_INFO = 'https://io.se.jisuanke.com/IO/GetUserAllInfoCtrl'
export const GET_USER_INFO = 'https://io.se.jisuanke.com/IO/GetUserInfoCtrl'
export const UPDATE_USER_INFO = 'https://io.se.jisuanke.com/IO/UpdateUserInfoCtrl'
export const VERCODE = 'https://io.se.jisuanke.com/IO/VerificationCodeCtrl'
export const IDENTITY = 'https://io.se.jisuanke.com/IO/UploadCtrl'
export const LIKE_UNLIKE = 'https://io.se.jisuanke.com/IO/isLikeCtrl'
export const JUDGE_STATE = 'https://io.se.jisuanke.com/IO/sendLikeStateCtrl'
export const SEND_STATE = 'https://io.se.jisuanke.com/IO/addLikeStateCtrl'
export const GET_AD_URL = 'https://io.se.jisuanke.com/IO/getAdvertisementsCtrl'
export const GET_POST_SPEC = 'https://io.se.jisuanke.com/IO/getOtherPostsCtrl'

export const columns = [
  '请选择省份', '北京', '天津', '河北', '山西', '内蒙古', '辽宁', '吉林', '黑龙江', '上海',
  '江苏', '浙江', '安徽', '福建', '江西', '山东', '河南', '湖北', '湖南', '广东', '广西',
  '海南', '重庆', '四川', '贵州', '云南', '西藏', '陕西',
  '甘肃', '青海', '宁夏', '新疆', '台湾', '香港', '澳门'
]

export const schoolColumn = [
  '合肥工业大学', '北京大学', '清华大学'
]

export const sortMethods = [
  '综合排序', '按发帖时间'
]

export const prizeList = [
  'NOIP普及组一等奖', 'NOIP普及组二等奖', 'NOIP普及组三等奖',
  'NOIP提高组一等奖', 'NOIP提高组二等奖', 'NOIP提高组三等奖',
  'NOI金牌', 'NOI银牌', 'NOI铜牌', 'IOI奖项', '未获上述奖项'
]
export default {

}
