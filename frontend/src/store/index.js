import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  posts: [],
  currentPost: undefined,
  openId: '',
  userCommentRight: 0,
  userProvince: '',
  relateFloor: 0,
  showReplyBox: false,
  comments: [],
  phone: '',
  adList: [],
  currentAd: undefined
}

const store = new Vuex.Store({
  state,
  mutations: {
    setCurrentPost (state, posts) {
      state.currentPost = undefined
      state.currentPost = posts
    },
    setPosts (state, posts) {
      state.posts = undefined
      let temp = posts
      for (let i = 0; i < temp.length; i++) {
        if (temp[i].is_fold === 1) {
          if (temp[i].uid !== state.openId) {
            temp.splice(i, 1)
          }
        }
      }
      state.posts = temp
    },
    addPosts (state, posts) {
      let temp = posts
      for (let i = 0; i < temp.length; i++) {
        if (temp[i].is_fold === 1) {
          if (temp[i].uid !== state.openId) {
            temp.splice(i, 1)
          }
        }
      }
      for (let i = 0; i < temp.length; i++) {
        state.posts.push(temp[i])
      }
    },
    setOpenId (state, id) {
      state.openId = id
    },
    setCommentRight (state, right) {
      state.userCommentRight = right
    },
    setUserProvince (state, province) {
      state.userProvince = province
    },
    setPhone (state, phone) {
      state.phone = phone
    },
    resetPosts (state) {
      state.posts = []
    },
    setRelateFloor (state, floor) {
      state.relateFloor = floor
    },
    setShowReplyBox (state, show) {
      state.showReplyBox = show
    },
    setComments (state, comments) {
      state.comments = comments
    },
    resetComments (state) {
      state.comments = []
    },
    addComments (state, comments) {
      for (let i = 0; i < comments.length; i++) {
        state.comments.push(comments[i])
      }
    },
    addCommentNum (state) {
      for (let i = 0; i < state.posts.length; i++) {
        if (state.posts[i].postid === state.currentPost.postid) {
          state.posts[i].comment = state.posts[i].comment + 1
          break
        }
      }
    },
    setAdList (state, ads) {
      state.adList = ads
    },
    setCurrentAd (state, ad) {
      state.currentAd = ad
    }
  }
})

export default store
