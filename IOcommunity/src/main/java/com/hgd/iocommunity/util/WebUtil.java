package com.hgd.iocommunity.util;

public class WebUtil {

  public static final int POST_MAX_PAGE_LINES = 5;
  
  public static final int COMMENT_MAX_PAGE_LINES = 5;
  
  public static final int ADVERTISEMENT_MAX_PAGE_LINES = 5;

  public static final int LIST_MAX_PAGE_LINES = 5;

  public String changetypeObject(String type) {
    if (type.equals("student")) {
      return "1";
    } else if (type.equals("institutionTeacher") 
        || type.equals("schoolTeacher")) {
      return "2";
    } else if (type.equals("parent")) {
      return "3";
    } else if (type.equals("other")) {
      return "4";
    }
    return "0";
  }
}
