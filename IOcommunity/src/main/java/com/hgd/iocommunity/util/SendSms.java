package com.hgd.iocommunity.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class SendSms {
  private static String URI_SEND_SMS = 
      "https://sms.yunpian.com/v2/sms/single_send.json";
  private static String ENCODING = "UTF-8";
    
  public static void sendsms(String phone, String text) 
      throws IOException, URISyntaxException {
    String apikey = "3de25875c09e0b598ed3f3a17bd6e1c1";
    String mobile = phone;
    SendSms.sendSms(apikey, text, mobile);
    long tplId = 1;
    String tplValue = URLEncoder.encode("#code#", ENCODING) 
        + "=" + URLEncoder.encode("1234", ENCODING) + "&" 
        + URLEncoder.encode("#company#", ENCODING) + "=" 
        + URLEncoder.encode("云片网", ENCODING);
  }
  
  public static String sendSms(String apikey, String text, 
      String mobile) throws IOException {
    Map<String, String> params = new HashMap<String, String>();
    params.put("apikey", apikey);
    params.put("text", text);
    params.put("mobile", mobile);
    return post(URI_SEND_SMS, params);
  }
  
  public static String post(String url, Map<String, String> paramsMap) {
    CloseableHttpClient client = HttpClients.createDefault();
    String responseText = "";
    CloseableHttpResponse response = null;
    try {
      HttpPost method = new HttpPost(url);
      if (paramsMap != null) {
        List<NameValuePair> paramList = new ArrayList<NameValuePair>();
        for (Map.Entry<String, String> param: paramsMap.entrySet()) {
          NameValuePair pair = new BasicNameValuePair(param.getKey(),
                  param.getValue());
          paramList.add(pair);
        }
        method.setEntity(new UrlEncodedFormEntity(paramList,
            ENCODING));
      }
      response = client.execute(method);
      HttpEntity entity = response.getEntity();
      if (entity != null) {
        responseText = EntityUtils.toString(entity, ENCODING);
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        response.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return responseText;
  }
}
