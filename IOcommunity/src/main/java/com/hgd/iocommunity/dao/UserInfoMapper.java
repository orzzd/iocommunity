package com.hgd.iocommunity.dao;

import com.hgd.iocommunity.domain.Other;
import com.hgd.iocommunity.domain.Parent;
import com.hgd.iocommunity.domain.Student;
import com.hgd.iocommunity.domain.Teacher;

import java.util.List;
import java.util.Map;

public interface UserInfoMapper {
  
  public void insertNewUser(Map<String,Object> map);
  
  public int inOtherTabel(String uid);
  
  public int inParentTabel(String uid);
  
  public int inStudentTabel(String uid);
  
  public int inTeacherTabel(String uid);
  
  public int isSchoolTeacher(String uid);
  
  public int isInstitutionTeacher(String uid);
  
  public Student queryBySid(String sid);
  
  public Other queryByOid(String oid);
  
  public Teacher queryByTid(String tid);
  
  public Parent queryByPid(String pid);
  
  public void deleteBySid(String uid);
  
  public void deleteByOid(String uid);
  
  public void deleteByTid(String uid);
  
  public void deleteByPid(String uid);
  
  public void deleteStudentAuthentication(String uid);
  
  public void deleteTeacherAuthentication(String uid);
  
  public void insertOther(Map<String,Object> map);
  
  public void insertStudent(Map<String,Object> map);
  
  public void insertTeacher(Map<String,Object> map);
  
  public void insertParent(Map<String,Object> map);
  
  public void banStudent(String uid);
  
  public void banTeacher(String uid);
  
  public void banParent(String uid);
  
  public void banOther(String uid);
  
  public void addAdmin(Map<String,Object> map);
  
  public List<String> getRangeByPhone (String phonenumber);
  
}