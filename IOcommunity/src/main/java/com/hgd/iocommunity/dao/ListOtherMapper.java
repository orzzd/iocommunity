package com.hgd.iocommunity.dao;

import com.hgd.iocommunity.domain.Other;

import java.util.List;
import java.util.Map;

public interface ListOtherMapper {
  
  public List<Other> queryByPage(Map<String,Object> map);
  
  public int queryMaxCount(Map<String,Object> map);
}
