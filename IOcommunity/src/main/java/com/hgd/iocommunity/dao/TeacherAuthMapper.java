package com.hgd.iocommunity.dao;

import com.hgd.iocommunity.domain.TeacherAuth;

import java.util.List;
import java.util.Map;

public interface TeacherAuthMapper {
  
  public List<TeacherAuth> queryAll();
  
  public List<TeacherAuth> queryByPage(Map<String,Object> map);
  
  public void deleteAuth(String picture);
  
  public void deleteAuthOfTeacher(String tid);

  public void addPrize(String institution, String tid);
}
