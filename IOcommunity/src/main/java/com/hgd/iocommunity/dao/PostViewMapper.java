package com.hgd.iocommunity.dao;

import com.hgd.iocommunity.domain.PostView;

import java.util.List;
import java.util.Map;

public interface PostViewMapper {
  
  public PostView queryById(String postid);
  
  public int queryCount(Map<String,Object> map);
  
  public List<PostView> queryByPage(Map<String,Object> map);
  
  public List<PostView> queryOtherByPage(Map<String,Object> map);
  
  public int queryCountByRange(Map<String,Object> map);
  
  public List<PostView> queryByPageAndRange(Map<String,Object> map);
}