package com.hgd.iocommunity.dao;

import com.hgd.iocommunity.domain.Student;

import java.util.List;
import java.util.Map;

public interface ListStudentMapper {
  
  public List<Student> queryByPage(Map<String,Object> map);
  
  public int queryMaxCount(Map<String,Object> map);
  
  public void addPrize(String prize, String sid);
}
