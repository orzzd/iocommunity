package com.hgd.iocommunity.dao;

import com.hgd.iocommunity.domain.Comment;

import java.util.Map;

public interface CommentMapper {
  public int insert(Comment comment);
  
  public int getMaxFloor(String postid);
  
  public int queryCountByPostid(String postid);
  
  public void delete(Map<String,Object> map);
  
  public void like(Map<String,Object> map);
  
  public void unlike(Map<String,Object> map);
}