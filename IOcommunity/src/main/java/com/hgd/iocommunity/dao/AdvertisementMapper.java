package com.hgd.iocommunity.dao;

import com.hgd.iocommunity.domain.Advertisement;

import java.util.List;
import java.util.Map;

public interface AdvertisementMapper {
  
  public List<Advertisement> queryByPage(Map<String,Object> map);
  
  public int queryMaxId();
  
  public int queryMaxCount(Map<String,Object> map);
  
  public List<Advertisement> queryAll();
  
  public int insert(Advertisement advertiserment);
  
  public void delete(int adid);
}

