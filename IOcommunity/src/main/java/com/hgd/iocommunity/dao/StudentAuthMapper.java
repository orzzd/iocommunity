package com.hgd.iocommunity.dao;

import com.hgd.iocommunity.domain.StudentAuth;

import java.util.List;
import java.util.Map;

public interface StudentAuthMapper {
  public List<StudentAuth> queryAll();
  
  public List<StudentAuth> queryByPage(Map<String,Object> map);
  
  public void deleteAuth(String picture);

  public void deleteAuthOfStudent(String sid);

  public void addPrize(String prize, String sid);
}

