package com.hgd.iocommunity.dao;

import com.hgd.iocommunity.domain.Parent;

import java.util.List;
import java.util.Map;

public interface ListParentMapper {
  
  public List<Parent> queryByPage(Map<String,Object> map);
  
  public int queryMaxCount(Map<String,Object> map);
}
