package com.hgd.iocommunity.dao;

import com.hgd.iocommunity.domain.CommentView;

import java.util.List;
import java.util.Map;

public interface CommentViewMapper {
  
  public List<CommentView> queryByPage(Map<String,Object> map);
  
  public int queryMaxCount(Map<String,Object> map);
  
  public List<CommentView> queryByPageAndPostid(Map<String,Object> map);
  
  public int queryCountByPostid(Map<String,Object> map);
}
