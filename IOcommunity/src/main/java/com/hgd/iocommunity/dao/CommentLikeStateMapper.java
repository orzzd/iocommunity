package com.hgd.iocommunity.dao;

import com.hgd.iocommunity.domain.CommentLikeState;

import java.util.Map;

public interface CommentLikeStateMapper {
  
  public int insert(CommentLikeState commentLikeState);
  
  public CommentLikeState selectCommentLikeState(Map<String, Object> map);
  
  public void like(Map<String, Object> map);
  
  public void unlike(Map<String, Object> map);
}