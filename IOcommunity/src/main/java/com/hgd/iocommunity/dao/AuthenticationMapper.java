package com.hgd.iocommunity.dao;

import java.util.Map;

public interface AuthenticationMapper {
  
  public void studentAuthentication(Map<String, Object> map);
  
  public void teacherAuthentication(Map<String, Object> map);
  
}
