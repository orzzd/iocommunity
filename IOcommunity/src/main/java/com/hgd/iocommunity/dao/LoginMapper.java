package com.hgd.iocommunity.dao;

public interface LoginMapper {
  
  public int isModularAdmin(String phonenumber);
  
  public int isSuperAdmin(String phonenumber);
}
