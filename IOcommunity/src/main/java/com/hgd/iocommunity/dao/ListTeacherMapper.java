package com.hgd.iocommunity.dao;

import com.hgd.iocommunity.domain.Teacher;

import java.util.List;
import java.util.Map;

public interface ListTeacherMapper {
  
  public List<Teacher> queryByPage(Map<String,Object> map);
  
  public int queryMaxCount(Map<String,Object> map);
}