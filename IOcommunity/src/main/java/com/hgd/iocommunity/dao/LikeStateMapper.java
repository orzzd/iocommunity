package com.hgd.iocommunity.dao;

import com.hgd.iocommunity.domain.LikeState;

import java.util.Map;

public interface LikeStateMapper {
  
  public int insert(LikeState likeState);
  
  public LikeState selectLikeState(Map<String, Object> map);
  
  public void like(Map<String, Object> map);
  
  public void unlike(Map<String, Object> map);
}
