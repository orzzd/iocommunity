package com.hgd.iocommunity.dao;

import com.hgd.iocommunity.domain.Post;

public interface PostMapper {
  
  public int insert(Post post);
  
  public void delete(String postid);
  
  public int queryMaxCount();
  
  public void fold(String postid);
  
  public void unfold(String postid);
  
  public void digest(String postid);
  
  public void nodigest(String postid);
  
  public void comment(String postid);
  
  public void nocomment(String postid);
  
  public void like(String postid);
  
  public void unlike(String postid);
  
  public void notLike(String postid);
  
  public void notUnlike(String postid);
  
  public void updateComment(String postid);
  
}
