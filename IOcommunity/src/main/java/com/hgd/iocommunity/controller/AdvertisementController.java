package com.hgd.iocommunity.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hgd.iocommunity.domain.Advertisement;
import com.hgd.iocommunity.service.AdvertisementService;
import com.hgd.iocommunity.state.PageQueryState;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AdvertisementController {
  @Resource
  private AdvertisementService advertisementService;
  
  @RequestMapping(value = "AddAdvertisementCtrl", method = RequestMethod.POST)
  public String addAdvertisement(@Valid @ModelAttribute("advertiserment")
      Advertisement advertiserment,  Errors errors) {
    int adid = advertisementService.getMaxId() + 1;
    Advertisement theAdvertiserment = new Advertisement(adid, 
        advertiserment.getVid(), advertiserment.getTitle());
    if (errors.hasFieldErrors()) {
      return "AddAdvertisement";
    }
    try {
      advertisementService.addAdvertisement(theAdvertiserment);
    } catch (Exception e) {
      e.printStackTrace();
      errors.reject("", "发生非预期错误，请联系管理员");
      return "AddAdvertisement";
    }
    return "redirect:ListAdvertisementsCtrl";
  }
  
  @RequestMapping(value = "DeleteAdvertisementCtrl")
  public String deleteAdvertisement(int adid) {
    advertisementService.deleteAdvertisement(adid);
    return "redirect:ListAdvertisementsCtrl";
  }
  
  @RequestMapping(value = "ListAdvertisementsCtrl", method = RequestMethod.GET)
  public String listAll(Model model, HttpSession session, String page) {
    PageQueryState state = null;
    if (page == null) {
      page = "0";
      session.removeAttribute("PageQueryState");
      state = new PageQueryState();
    } else {
      state = (PageQueryState)session.getAttribute("PageQueryState");
      if (state == null) {
        state = new PageQueryState();
      }
    }
    
    List<Advertisement> list = null;
    try {
      int lastPage = advertisementService.getLastPage(state);
      state.setLastPage(lastPage);
      
      list = advertisementService.getAdvertisementsByPage(state, page);
      session.setAttribute("PageQueryState", state);
      model.addAttribute("lastPage", lastPage);
    } catch (Exception e) {
      e.printStackTrace();
      list = new ArrayList<Advertisement>();
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "发生非预期错误，请联系管理员");
      model.addAttribute("errMap", errMap);
    }
    
    model.addAttribute("listAdvertisements", list); 
    return "admin/ManageAdvertisement";
  }
  
  @RequestMapping(value = "getAdvertisementsCtrl", method = RequestMethod.POST)
  public void getAll(HttpServletRequest request, HttpServletResponse response)
      throws IOException {
    List<Advertisement> list = advertisementService.getAllAdvertisements();
    Writer out = response.getWriter();
    JSONArray jsonarray = new JSONArray();
    for (int i = 0; i < list.size(); i++) {
      Advertisement advertisement = list.get(i);
      JSONObject jsonobject = new JSONObject();
      jsonobject.put("vid", advertisement.getVid());
      jsonobject.put("title", advertisement.getTitle());
      jsonarray.add(jsonobject);
    }
    out.write(jsonarray.toJSONString());
    out.flush();
  }
  
}
