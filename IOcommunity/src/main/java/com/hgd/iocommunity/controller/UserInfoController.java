package com.hgd.iocommunity.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.exceptions.ClientException;
import com.hgd.iocommunity.domain.Other;
import com.hgd.iocommunity.domain.Parent;
import com.hgd.iocommunity.domain.Student;
import com.hgd.iocommunity.domain.Teacher;
import com.hgd.iocommunity.service.LoginService;
import com.hgd.iocommunity.service.UserInfoService;
import com.hgd.iocommunity.util.SendSms;
import com.hgd.iocommunity.util.WebUtil;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserInfoController {
  @Resource
  private UserInfoService userInfoSvc;
  
  @Resource
  private LoginService loginSvc;
  
  String verifyCode;
  String userPhone;
  String oldPhone;
  
  @RequestMapping(value = "InsertNewUserCtrl", method = RequestMethod.POST)
  private void insertNewUser(HttpServletRequest request, HttpServletResponse 
      response) throws ServletException, IOException {
    String uid = request.getParameter("uid");
    String nickname = request.getParameter("nickname");
    String icon = request.getParameter("icon");
    String province = request.getParameter("province");
    String type = userInfoSvc.usertype(uid);
    if (type.equals("tourist")) {
      userInfoSvc.insertNewUser(uid, nickname, province, icon);
    }
  }
  
  @RequestMapping(value = "UpdateUserInfoCtrl", method = RequestMethod.POST)
  private void updateUserInfo(HttpServletRequest request, HttpServletResponse 
      response) throws ServletException, IOException {
    Map<String, Object> map = new HashMap<>();
    String uid = request.getParameter("uid");
    String nickname = request.getParameter("nickname");
    String phonenumber = request.getParameter("phone");
    String icon = request.getParameter("icon");
    String province = request.getParameter("province");
    String banString = request.getParameter("ban");
    int ban = Integer.parseInt(banString);
    String verificationCode = request.getParameter("verificationCode");
    String type = request.getParameter("type");
    String oldtype = userInfoSvc.usertype(uid);
    String typenumber = new WebUtil().changetypeObject(oldtype);
    JSONObject successJson = new JSONObject();
    if ((verificationCode.equals(verifyCode) 
        && userPhone.equals(phonenumber)) || phonenumber.equals(oldPhone)) {
      map.put("nickname",nickname);
      map.put("phonenumber",phonenumber);
      map.put("icon",icon);
      map.put("ban",ban);
      map.put("province",province);
      if (typenumber.equals("1")) {
        userInfoSvc.deleteStudentAuthentication(uid);
        userInfoSvc.deleteBySid(uid);
      } else if (typenumber.equals("2")) {
        userInfoSvc.deleteTeacherAuthentication(uid);
        userInfoSvc.deleteByTid(uid);
      } else if (typenumber.equals("3")) {
        userInfoSvc.deleteByPid(uid);
      } else if (typenumber.equals("4")) {
        userInfoSvc.deleteByOid(uid);
      }
      if (type.equals("1")) {
        String school = request.getParameter("school");
        String prize = request.getParameter("prize");
        map.put("sid",uid);
        map.put("school",school);
        map.put("prize","未认证");
        userInfoSvc.insertStudent(map);
      } else if (type.equals("2")) {
        String teachertype = request.getParameter("teachertype");
        map.put("tid",uid);
        map.put("institution","未认证");
        map.put("teachertype",teachertype);
        userInfoSvc.insertTeacher(map);
      } else if (type.equals("3")) { 
        String objective = request.getParameter("objective");
        map.put("pid",uid);
        map.put("objective",objective);
        userInfoSvc.insertParent(map);
      } else if (type.equals("4")) {
        map.put("oid",uid);
        userInfoSvc.insertOther(map); 
      }
      successJson.put("success",1);
      
    } else {
      successJson.put("success",0);
    }
    successJson.put("vcode", verifyCode);
    successJson.put("verificationCode", verificationCode);
    successJson.put("phone", userPhone);
    successJson.put("phonenumber", phonenumber);
    Writer out = response.getWriter(); 
    String successString = JSON.toJSONString(successJson);
    out.write(successString);
    out.flush();
  }

  @RequestMapping(value = "GetUserInfoCtrl", method = RequestMethod.POST)
  private void getUserInfo(HttpServletRequest request, HttpServletResponse 
      response) throws ServletException, IOException {
    String uid = request.getParameter("uid");
    String type = userInfoSvc.usertype(uid);
    String nickname = "tourist";
    String phonenumber = "未绑定手机号";
    String province = "北京";
    JSONObject userinfoJson = new JSONObject();
    if (type.equals("student")) {
      Student student = userInfoSvc.getStudentInfo(uid);
      nickname = student.getNickname();
      phonenumber = student.getPhonenumber();
      province = student.getProvince();
      String prize = student.getPrize();
      userinfoJson.put("prize",prize);
    } else if (type.equals("institutionTeacher") || type.equals("schoolTeacher")) {
      Teacher teacher = userInfoSvc.getTeacherInfo(uid);
      nickname = teacher.getNickname();
      phonenumber = teacher.getPhonenumber();
      province = teacher.getProvince();
      String institution = teacher.getInstitution();
      userinfoJson.put("institution",institution);
    } else if (type.equals("other")) {
      Other other = userInfoSvc.getOtherInfo(uid);
      nickname = other.getNickname();
      phonenumber = other.getPhonenumber();
      province = other.getProvince();
    } else if (type.equals("parent")) {
      Parent parent = userInfoSvc.getParentInfo(uid);
      nickname = parent.getNickname();
      phonenumber = parent.getPhonenumber();
      province = parent.getProvince();
    }
    userinfoJson.put("uid",uid);
    userinfoJson.put("nickname",nickname);
    userinfoJson.put("phonenumber",phonenumber);
    userinfoJson.put("province",province);
    userinfoJson.put("type",type);
    String userinfo = userinfoJson.toJSONString();
    Writer out = response.getWriter(); 
    out.write(userinfo);
    out.flush();
  }
  
  @RequestMapping(value = "GetUserAllInfoCtrl", method = RequestMethod.POST)
  private void getUserAllInfo(HttpServletRequest request, HttpServletResponse 
      response) throws ServletException, IOException {
    String uid = request.getParameter("uid");
    String type = userInfoSvc.usertype(uid);
    String item = "";
    if (type.equals("student")) {
      Student student = userInfoSvc.getStudentInfo(uid);
      oldPhone = student.getPhonenumber();
      item = JSON.toJSONString(student);
    } else if (type.equals("institutionTeacher") || type.equals(
        "schoolTeacher")) {
      Teacher teacher = userInfoSvc.getTeacherInfo(uid);
      oldPhone = teacher.getPhonenumber();
      item = JSON.toJSONString(teacher);
    } else if (type.equals("other")) {
      Other other = userInfoSvc.getOtherInfo(uid);
      oldPhone = other.getPhonenumber();
      item = JSON.toJSONString(other);
    } else if (type.equals("parent")) {
      Parent parent = userInfoSvc.getParentInfo(uid);
      oldPhone = parent.getPhonenumber();
      item = JSON.toJSONString(parent);
    }
    String typenumber = new WebUtil().changetypeObject(type);
    Writer out = response.getWriter(); 
    JSONObject userinfoJson = JSON.parseObject(item);
    userinfoJson.put("type",typenumber);
    String userinfo = userinfoJson.toJSONString();
    out.write(userinfo);
    out.flush();
  }  
  
  @RequestMapping(value = "VerificationCodeCtrl", method = RequestMethod.POST)
  private void verificationCode(HttpServletRequest request, 
      HttpServletResponse response) throws ServletException, 
      IOException, ClientException, URISyntaxException {
    userPhone = request.getParameter("phone");
    verifyCode = String.valueOf(new Random().nextInt(899999) + 100000);
    String text = "【信奥社区】小老弟，这个码你拿去 " + verifyCode + " ，麻利点";
    new SendSms().sendsms(userPhone,text);
  }
   
  @RequestMapping(value = "LoginCtrl", method = RequestMethod.GET)
  private void login(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException {
    String appid = request.getParameter("appid");
    String secret = request.getParameter("secret");
    String jsCode = request.getParameter("js_code");
    String grantType = request.getParameter("grant_type");
    String url = "https://api.weixin.qq.com/sns/jscode2session";
    URL urlHttp = new URL(url);
    HttpURLConnection connection = (HttpURLConnection) urlHttp
        .openConnection();
    connection.setRequestProperty("Content-Type", 
        "application/x-www-form-urlencoded;charset=utf-8");
    connection.setRequestMethod("POST");
    connection.setDoOutput(true);
    connection.connect();
    DataOutputStream dataout = new DataOutputStream(connection
        .getOutputStream());
    
    String appidParam = "appid=" + appid;
    String secretParam = "&secret=" + secret;
    String jsCodeParam = "&js_code=" + jsCode;
    String grantTypeParam = "&grant_type=" + grantType;
    String param = appidParam + secretParam + jsCodeParam + grantTypeParam;
    dataout.writeBytes(param);
    dataout.flush();
    dataout.close();
    BufferedReader bf = new BufferedReader(
        new InputStreamReader(connection.getInputStream(), "UTF-8"));
    String line;
    StringBuilder sb = new StringBuilder();
    while ((line = bf.readLine()) != null) {
      sb.append(line).append(System.getProperty("line.separator"));
    }
    bf.close();
    connection.disconnect();
    String sbString = sb.toString();
    JSONObject sbJson = JSON.parseObject(sbString);
    sbJson.put("appid", appid);
    sbJson.put("secret", secret);
    sbJson.put("js_code", jsCode);
    sbJson.put("grant_type", grantType);
    sbJson.put("text", "123");
    Writer out = response.getWriter();
    String writesb = JSON.toJSONString(sbJson);
    out.write(writesb);
    out.flush();
  }
  
  @RequestMapping(value = "BanUserCtrl", method = RequestMethod.GET)
  private String banUser(Model model, HttpSession session, String uid) 
      throws ServletException {
    String type = userInfoSvc.usertype(uid);
    if (type.equals("student")) {
      userInfoSvc.banStudent(uid);
      return "redirect:ListStudentsCtrl";
    } else if (type.equals("institutionTeacher") 
        || type.equals("schoolTeacher")) {
      userInfoSvc.banTeacher(uid);
      return "redirect:ListTeacherCtrl";
    } else if (type.equals("parent")) {
      userInfoSvc.banParent(uid);
      return "redirect:ListParentCtrl";
    } else {
      userInfoSvc.banOther(uid);
      return "redirect:ListOtherCtrl";
    }
  }
  
}
  
