package com.hgd.iocommunity.controller;

import com.hgd.iocommunity.domain.Student;
import com.hgd.iocommunity.domain.StudentAuth;
import com.hgd.iocommunity.service.StudentAuthService;
import com.hgd.iocommunity.service.UserInfoService;
import com.hgd.iocommunity.util.SendSms;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class StudentAuthController {
  @Resource
  private StudentAuthService studentAuthServ;
  
  @Resource
  private UserInfoService userInfoSvc;
  
  @RequestMapping(value = "/ListStudentAuthCtrl", method = RequestMethod.GET)
  public String listAll(Model model, HttpSession session) {
    List<StudentAuth> list = null;
    try {
      list = studentAuthServ.getAuthList();
      model.addAttribute("StudentAuthList", list);
    } catch (Exception e) {
      e.printStackTrace();
    }
    
    return "admin/StudentAuth";
  }
  
  @RequestMapping(value = "/deleteAuthCtrl", method = RequestMethod.GET)
  public String delete(String sid) 
      throws IOException, URISyntaxException {
    studentAuthServ.deleteAuthOfStudent(sid);
    Student student = userInfoSvc.getStudentInfo(sid);
    String text = "【信奥社区】对不起！您的认证未通过，请确认认证信息之后重新提交";
    new SendSms().sendsms(student.getPhonenumber(),text);
    return "redirect:/ListStudentAuthCtrl";
  }
  
  @RequestMapping(value = "/successAuthCtrl", method = RequestMethod.GET)
  public String success(String sid,String prize) 
      throws IOException, URISyntaxException {
    System.out.println(sid);
    System.out.println(prize);
    studentAuthServ.success(sid, prize);
    Student student = userInfoSvc.getStudentInfo(sid);
    String text = "【信奥社区】您的认证已通过，请登录小程序查看！";
    new SendSms().sendsms(student.getPhonenumber(),text);
    return "redirect:/ListStudentAuthCtrl";
  }
}
