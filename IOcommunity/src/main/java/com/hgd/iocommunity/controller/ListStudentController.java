package com.hgd.iocommunity.controller;

import com.hgd.iocommunity.domain.Student;
import com.hgd.iocommunity.service.ListStudentService;
import com.hgd.iocommunity.state.PageQueryState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ListStudentController {
  @Resource
  private ListStudentService listStudentService;
  
  @RequestMapping(value = "ListStudentsCtrl", method = RequestMethod.GET)
  public String listStudents(Model model, HttpSession session, String page) {
    PageQueryState state = null;
    if (page == null) {
      page = "0";
      session.removeAttribute("PageQueryState");
      state = new PageQueryState();
    } else {
      state = (PageQueryState)session.getAttribute("PageQueryState");
      if (state == null) {
        state = new PageQueryState();
      }
    }
    
    List<Student> list = null;
    try {
      int lastPage = listStudentService.getLastPage(state);
      state.setLastPage(lastPage);
      
      list = listStudentService.getStudentsByPage(state, page);
      session.setAttribute("PageQueryState", state);
      model.addAttribute("lastPage", lastPage);
    } catch (Exception e) {
      e.printStackTrace();
      list = new ArrayList<Student>();
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "发生非预期错误，请联系管理员");
      model.addAttribute("errMap", errMap);
    }
    
    model.addAttribute("listStudents", list); 
    return "admin/ManageStudent";    
  }
  
  @RequestMapping(value = "ListStudentsAdminCtrl", method = RequestMethod.GET)
  public String listStudentsAdmin(Model model, HttpSession session, 
      String page) {
    PageQueryState state = null;
    if (page == null) {
      page = "0";
      session.removeAttribute("PageQueryState");
      state = new PageQueryState();
    } else {
      state = (PageQueryState)session.getAttribute("PageQueryState");
      if (state == null) {
        state = new PageQueryState();
      }
    }
    
    List<Student> list = null;
    try {
      int lastPage = listStudentService.getLastPage(state);
      state.setLastPage(lastPage);
      
      list = listStudentService.getStudentsByPage(state, page);
      session.setAttribute("PageQueryState", state);
      model.addAttribute("lastPage", lastPage);
    } catch (Exception e) {
      e.printStackTrace();
      list = new ArrayList<Student>();
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "发生非预期错误，请联系管理员");
      model.addAttribute("errMap", errMap);
    }
    
    model.addAttribute("listStudents", list); 
    return "admin/AddManagerStudent";    
  }
}
