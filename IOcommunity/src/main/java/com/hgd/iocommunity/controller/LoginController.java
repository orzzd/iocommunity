package com.hgd.iocommunity.controller;

import com.hgd.iocommunity.service.LoginService;
import com.hgd.iocommunity.util.SendSms;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {
  
  @Resource
  private LoginService loginSvc;
  
  @RequestMapping(value = "AdminVerifyCodeCtrl", method = RequestMethod.POST)
  public String sendAdminVerifyCode(HttpServletRequest request, 
      HttpSession session) throws IOException, URISyntaxException {
    String adminVerifyCode = String.valueOf(new Random().nextInt(899999) 
        + 100000);
    String phonenumber = (String) request.getParameter("phonenumber");
    String adminType = loginSvc.admintype(phonenumber);
    if (adminType.equals("modularAdmin") || adminType.equals("superAdmin")) {
      session.setAttribute("loginPhonenumber", phonenumber);
      String text = "【信奥社区】小老弟，这个码你拿去 " + adminVerifyCode + " ，麻利点";
      new SendSms().sendsms(phonenumber,text);
      session.setAttribute("adminVerifyCode", adminVerifyCode);
    } else {
      session.setAttribute("usererror", "请输入正确的手机号");
    }
    return "index";
  }
  
  @RequestMapping(value = "AdminLoginCtrl", method = RequestMethod.POST)
  public String adminLogin(HttpServletRequest request, HttpSession session) 
      throws IOException, URISyntaxException {
    String phonenumber = (String) request.getParameter("phonenumber");
    String adminType = loginSvc.admintype(phonenumber);
    String adminVerifyCode = (String) session.getAttribute("adminVerifyCode");
    String verifyCode = request.getParameter("VerifyCode");
    if (adminType.equals("modularAdmin") && verifyCode.equals(
        adminVerifyCode)) {
      return "redirect:ListCountryPostsModularCtrl";
    } else if (adminType.equals("superAdmin") && verifyCode.equals(
        adminVerifyCode)) {
      return "redirect:ListCountryPostsCtrl";
    }
    if (!verifyCode.equals(adminVerifyCode)) {
      session.setAttribute("usererror", "请输入正确的手机号或验证码");
    }
    return "index";
  }
  

  @RequestMapping(value = "AdminLogoutCtrl", method = RequestMethod.GET)
  public String adminLogout(HttpServletRequest request, HttpSession session) 
      throws IOException, URISyntaxException {
    session.removeAttribute("loginPhonenumber");
    return "index";
  }

}
