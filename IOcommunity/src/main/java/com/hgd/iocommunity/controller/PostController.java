package com.hgd.iocommunity.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hgd.iocommunity.domain.LikeState;
import com.hgd.iocommunity.domain.Post;
import com.hgd.iocommunity.domain.PostView;
import com.hgd.iocommunity.service.LikeStateService;
import com.hgd.iocommunity.service.PostService;
import com.hgd.iocommunity.service.UserInfoService;
import com.hgd.iocommunity.state.PostListQueryState;
import com.hgd.iocommunity.state.PostQueryState;

import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PostController {
  @Resource
  private PostService postService;
  @Resource
  private PostService postViewService;
  @Resource
  private LikeStateService likeStateService;
  @Resource
  private UserInfoService userInfoSvc;
  
  public String generatePostid() {
    String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijk"
        + "lmnopqrstuvwxyz1234567890";
    Random random = new Random();
    char []text = new char[20];
    for (int i = 0; i < 20; i++) {
      text[i] = characters.charAt(random.nextInt(characters.length()));
    }
    String str = new String(text);
    if (postService.getPost(str) == null && str != null) {
      return str;
    } else {
      return generatePostid();
    }    
  }
  
  @RequestMapping(value = "AddPostCtrl", method = RequestMethod.POST)
  public String addPost(@Valid @ModelAttribute("post")Post post, 
      Errors errors) {
    Post thePost = new Post();
    thePost.setPostid(generatePostid());
    thePost.setTitle(post.getTitle());
    thePost.setUid(post.getUid());
    thePost.setRange(post.getRange());
    thePost.setContent(post.getContent());
    thePost.setCommentAllowed(0);
    thePost.setIsEssence(0);
    thePost.setIsFold(0);
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    thePost.setDate(df.format(new Date()));
    thePost.setComment(0);
    thePost.setThumbUp(0);
    thePost.setThumbDown(0);
    thePost.setClickRate(0);
    if (errors.hasFieldErrors()) {
      return "AddPost";
    }
    try {
      postService.addPost(thePost);
    } catch (Exception e) {
      e.printStackTrace();
      errors.reject("", "发生非预期错误，请联系管理员");
      return "AddPost";
    }
    return "admin/AddPostSucc";
  }
  
  @RequestMapping(value = "sendPostCtrl",  method = RequestMethod.POST)
  public void sendPost(HttpServletRequest request, HttpServletResponse 
      response) {
    String title = request.getParameter("title");
    String uid = request.getParameter("uid");
    String range = request.getParameter("range");
    String content = request.getParameter("content");
    Post thePost = new Post();
    String postid = generatePostid();
    thePost.setPostid(postid);
    thePost.setTitle(title);
    thePost.setUid(uid);
    thePost.setRange(range);
    thePost.setContent(content);
    thePost.setCommentAllowed(0);
    thePost.setIsEssence(0);
    thePost.setIsFold(0);
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    thePost.setDate(df.format(new Date()));
    thePost.setComment(0);
    thePost.setThumbUp(0);
    thePost.setThumbDown(0);
    thePost.setClickRate(0);
    try {
      int post = postService.addPost(thePost);
      JSONObject jsonObject = new JSONObject();
      if (post == 0) {
        jsonObject.put("success", 0);      
        try {
          Writer out = response.getWriter();
          out.write(jsonObject.toJSONString());
          out.flush();
        } catch (IOException e1) {
          e1.printStackTrace();
        }
      } else {
        jsonObject.put("success", 1);
        Writer out;
        try {
          out = response.getWriter();
          out.write(jsonObject.toJSONString());
          out.flush();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  @RequestMapping(value = "DeletePostCtrl")
  public String deletePost(Model model, HttpServletRequest request, HttpSession session) {
    String items = request.getParameter("delitems");
    String[] strs = items.split(",");
 
    for (int i = 0; i < strs.length; i++) {
      try {
        String postid = strs[i];
        postService.deletePost(postid);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return "redirect:ListCountryPostsCtrl";
  }
  
  @RequestMapping(value = "DeletePostModularCtrl")
  public String deletePostModular(Model model, HttpSession session, String postid) {
    try {
      postService.deletePost(postid);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "redirect:ListCountryPostsModularCtrl";
  }
  
  @RequestMapping(value = "ListCountryPostsCtrl", method = RequestMethod.GET)
  public String listCountryAll(Model model, HttpSession session, String page) {
    PostQueryState state = null;
    if (page == null) {
      page = "0";
      session.removeAttribute("PostQueryState");
      state = new PostQueryState();
    } else {
      state = (PostQueryState)session.getAttribute("PostQueryState");
      if (state == null) {
        state = new PostQueryState();
      }
    }
    
    List<PostView> list = null;
    try {
      int lastPage = postViewService.getLastPage(state);
      state.setLastPage(lastPage);
      
      list = postViewService.getPostsByPage(state, page);
      session.setAttribute("PostQueryState", state);
      model.addAttribute("lastPage", lastPage);
    } catch (Exception e) {
      e.printStackTrace();
      list = new ArrayList<PostView>();
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "发生非预期错误，请联系管理员");
      model.addAttribute("errMap", errMap);
    }
    
    model.addAttribute("listCountryPosts", list); 
    return "admin/PostManage";
  }
  
  @RequestMapping(value = "ListCountryPostsCtrl", method = RequestMethod.POST)
  public String listBy(Model model, HttpSession session, String type, 
      String sortMethod) {
    session.removeAttribute("PostQueryState");
    PostQueryState state = new PostQueryState(0, type, sortMethod);
    
    List<PostView> list = null;
    try {
      int lastPage = postViewService.getLastPage(state);
      state.setLastPage(lastPage);
      
      list = postViewService.getPosts(state);
      session.setAttribute("PostQueryState", state);
      model.addAttribute("lastPage", lastPage);
    } catch (Exception e) {
      e.printStackTrace();
      list = new ArrayList<PostView>();
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "发生非预期错误，请联系管理员");
      model.addAttribute("errMap", errMap);
    }
    
    model.addAttribute("listCountryPosts", list); 
    return "admin/PostManage";
  }
  
  
  @RequestMapping(value = "ListCountryPostsModularCtrl", method = RequestMethod.GET)
  public String listCountryAllMOdular(Model model, HttpSession session, String page) {
    PostListQueryState state = null;
    String phonenumber = (String) session.getAttribute("loginPhonenumber");
    List<String> rangeList = userInfoSvc.getRangeByPhone(phonenumber);
    String range1 = null;
    String range2 = null;
    if(rangeList.size() == 1) {
      range1 = rangeList.get(0);
    } else if(rangeList.size() == 2) {
      range1 = rangeList.get(0);
      range2 = rangeList.get(1);
    }
    if (page == null) {
      page = "0";
      session.removeAttribute("PostListQueryState");
      state = new PostListQueryState(0, range1, range2);
    } else {
      state = (PostListQueryState)session.getAttribute("PostListQueryState");
      if (state == null) {
        state = new PostListQueryState(0, range1, range2);
      }
    }
    
    List<PostView> list = null;
    try {
      int lastPage = postViewService.getLastPageByRange(state);
      state.setLastPage(lastPage);
      
      list = postViewService.getPostsByPageAndRange(state, page);
      session.setAttribute("PostListQueryState", state);
      model.addAttribute("lastPage", lastPage);
    } catch (Exception e) {
      e.printStackTrace();
      list = new ArrayList<PostView>();
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "发生非预期错误，请联系管理员");
      model.addAttribute("errMap", errMap);
    }
    
    model.addAttribute("listCountryPosts", list); 
    return "admin/ModularAdmin";
  }
  
  @RequestMapping(value = "ListCountryPostsModularCtrl", method = RequestMethod.POST)
  public String listByModular(Model model, HttpSession session, String type, String sortMethod) {
    session.removeAttribute("PostQueryState");
    PostQueryState state = new PostQueryState(0, type, sortMethod);
    
    List<PostView> list = null;
    try {
      int lastPage = postViewService.getLastPage(state);
      state.setLastPage(lastPage);
      
      list = postViewService.getPosts(state);
      session.setAttribute("PostQueryState", state);
      model.addAttribute("lastPage", lastPage);
    } catch (Exception e) {
      e.printStackTrace();
      list = new ArrayList<PostView>();
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "发生非预期错误，请联系管理员");
      model.addAttribute("errMap", errMap);
    }
    
    model.addAttribute("listCountryPosts", list); 
    return "admin/ModularAdmain";
  }
  
  @RequestMapping(value = "getPostsCtrl", method = RequestMethod.POST)
  public void getPosts(HttpServletRequest request, HttpServletResponse response)
      throws IOException {
    String type = request.getParameter("type");
    String pageStr = request.getParameter("page");
    String sortMethod = request.getParameter("sort");
    int page = Integer.parseInt(pageStr);
    PostQueryState state = new PostQueryState(page, type, sortMethod);
    List<PostView> list = postViewService.getPosts(state);
    JSONArray jsonarray = new JSONArray();
    int size = 0;
    if (type == "热门") {
      size = 20;
    } else {
      size = list.size();
    }

    for (int i = 0; i < size; i++) {
      PostView post = list.get(i);
      String date = post.getDate().substring(0, 10);
      JSONObject jsonobject = new JSONObject();
      jsonobject.put("uid", post.getUid());
      jsonobject.put("nickname", post.getNickname());
      jsonobject.put("avatar", post.getIcon());
      jsonobject.put("range", post.getRange());
      jsonobject.put("date", date);
      jsonobject.put("title", post.getTitle());
      jsonobject.put("content", post.getContent());
      jsonobject.put("thumbsup", post.getThumbUp());
      jsonobject.put("thumbsdown", post.getThumbDown());
      jsonobject.put("comment", post.getComment());
      jsonobject.put("postid", post.getPostid());
      jsonobject.put("is_essence", post.getIsEssence());
      jsonobject.put("is_fold", post.getIsFold());
      jsonobject.put("comment_allowed", post.getCommentAllowed());
      if (post.getAuthentication() != "未认证" && post.getAuthentication() != null) {
        jsonobject.put("authentication", post.getAuthentication());
      }
      jsonarray.add(jsonobject);
    }
    Writer out = response.getWriter(); 
    out.write(jsonarray.toJSONString());
    out.flush(); 
  }
  
  @RequestMapping(value = "getOtherPostsCtrl", method = RequestMethod.POST)
  public void getOtherPosts(HttpServletRequest request, HttpServletResponse 
      response) throws IOException {
    String type = request.getParameter("type");
    String pageStr = request.getParameter("page");
    String sortMethod = request.getParameter("sort");
    int page = Integer.parseInt(pageStr);
    PostQueryState state = new PostQueryState(page, type, sortMethod);
    List<PostView> list = postViewService.getOtherPosts(state);
    JSONArray jsonarray = new JSONArray();
    for (int i = 0; i < list.size(); i++) {
      PostView post = list.get(i);
      String date = post.getDate().substring(0, 10);
      JSONObject jsonobject = new JSONObject();
      jsonobject.put("uid", post.getUid());
      jsonobject.put("nickname", post.getNickname());
      jsonobject.put("avatar", post.getIcon());
      jsonobject.put("range", post.getRange());
      jsonobject.put("date", date);
      jsonobject.put("title", post.getTitle());
      jsonobject.put("content", post.getContent());
      jsonobject.put("thumbsup", post.getThumbUp());
      jsonobject.put("thumbsdown", post.getThumbDown());
      jsonobject.put("comment", post.getComment());
      jsonobject.put("postid", post.getPostid());
      jsonobject.put("is_essence", post.getIsEssence());
      jsonobject.put("is_fold", post.getIsFold());
      jsonobject.put("comment_allowed", post.getCommentAllowed());
      if (post.getAuthentication() != "未认证" && post.getAuthentication() != null) {
        jsonobject.put("authentication", post.getAuthentication());
      }
      jsonarray.add(jsonobject);
    }
    Writer out = response.getWriter(); 
    out.write(jsonarray.toJSONString());
    out.flush(); 
  }
  
  @RequestMapping(value = "FoldPostCtrl")
  public String foldPost(Model model, HttpServletRequest request, HttpSession session) {
    String items = request.getParameter("delitems");
    String[] strs = items.split(",");
 
    for (int i = 0; i < strs.length; i++) {
      try {
        String postid = strs[i];
        postService.fold(postid);;
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return "redirect:ListCountryPostsCtrl";
  }
  
  @RequestMapping(value = "UnfoldPostCtrl")
  public String unfoldPost(Model model, HttpSession session, String postid) {
    try {
      postService.unfold(postid);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "redirect:ListCountryPostsCtrl";
  }
  
  @RequestMapping(value = "DigestPostCtrl")
  public String digestPost(Model model, HttpServletRequest request, HttpSession session) {
    String items = request.getParameter("delitems");
    String[] strs = items.split(",");
 
    for (int i = 0; i < strs.length; i++) {
      try {
        String postid = strs[i];
        postService.digest(postid);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return "redirect:ListCountryPostsCtrl";
  }
  
  @RequestMapping(value = "NodigestPostCtrl")
  public String nodigestPost(Model model, HttpSession session, String postid) {
    try {
      postService.nodigest(postid);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "redirect:ListCountryPostsCtrl";
  }
  
  @RequestMapping(value = "CommentPostCtrl")
  public String commentPost(Model model, HttpSession session, String postid) {
    try {
      postService.comment(postid);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "redirect:ListCountryPostsCtrl";
  }
  
  @RequestMapping(value = "NocommentPostCtrl")
  public String nocommentPost(Model model, HttpSession session, 
      String postid) {
    try {
      postService.nocomment(postid);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "redirect:ListCountryPostsCtrl";
  }
  
  @RequestMapping(value = "isLikeCtrl", method = RequestMethod.POST)
  public void isLike(HttpServletRequest request, HttpServletResponse 
      response) {
    String uid = request.getParameter("uid");
    String postid = request.getParameter("id");
    String likeStr = request.getParameter("like");
    String unlikeStr = request.getParameter("unlike");
    LikeState state = likeStateService.selectLikeState(postid, uid);
    int like = Integer.parseInt(likeStr);
    int unlike = Integer.parseInt(unlikeStr);
    int theLike, theUnlike;
    if (state == null) {
      theLike = 1;
      theUnlike = 1;
    } else {
      theLike = state.getLike();
      theUnlike = state.getUnlike();
    }
    if (theLike == 0 && like == 1) {
      postService.notLike(postid);
    }
    if (theLike == 1 && like == 0) {
      postService.like(postid);
    }
    if (theUnlike == 1 && unlike == 0) {
      postService.unlike(postid);
    }
    if (theUnlike == 0 && unlike == 1) {
      postService.notUnlike(postid);
    }
  }
  
  @RequestMapping(value = "IsLikeCtrl")
  public String isLike(String postid, int like, int unlike) {
    Map<String, Object> map = new HashMap<>();
    map.put("postid", postid);
    if (like == 0) {
      postService.like(postid);
    }
    if (unlike == 0) {
      postService.unlike(postid);
    }
    return "admin/LikeSucc";
  }
  
  @RequestMapping(value = "FoldPostModularCtrl")
  public String foldPostModular(Model model, HttpSession session, String postid) {
    try {
      postService.fold(postid); 
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "redirect:ListCountryPostsModularCtrl";
  }
  
  @RequestMapping(value = "UnfoldPostModularCtrl")
  public String unfoldPostModular(Model model, HttpSession session, String postid) {
    try {
      postService.unfold(postid);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "redirect:ListCountryPostsModularCtrl";
  }
  
  @RequestMapping(value = "DigestPostModularCtrl")
  public String digestPostModular(Model model, HttpSession session, String postid) {
    try {
      postService.digest(postid);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "redirect:ListCountryPostsModularCtrl";
  }
  
  @RequestMapping(value = "NodigestPostModularCtrl")
  public String nodigestPostModular(Model model, HttpSession session, String postid) {
    try {
      postService.nodigest(postid);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "redirect:ListCountryPostsModularCtrl";
  }
  
  @RequestMapping(value = "CommentPostModularCtrl")
  public String commentPostModular(Model model, HttpSession session, String postid) {
    try {
      postService.comment(postid);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "redirect:ListCountryPostsModularCtrl";
  }
  
  @RequestMapping(value = "NocommentPostModularCtrl")
  public String nocommentPostModular(Model model, HttpSession session, String postid) {
    try {
      postService.nocomment(postid);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "redirect:ListCountryPostsModularCtrl";
  }
}