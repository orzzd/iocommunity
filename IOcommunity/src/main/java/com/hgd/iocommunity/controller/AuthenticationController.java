package com.hgd.iocommunity.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hgd.iocommunity.service.AuthenticationService;
import com.hgd.iocommunity.service.UserInfoService;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.net.URISyntaxException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class AuthenticationController {
  @Resource
  private AuthenticationService authenticationSvc;
  
  @Resource
  private UserInfoService userInfoSvc;
 
  @RequestMapping("UploadCtrl")
    public void upload(HttpServletRequest request, HttpServletResponse response,
        @RequestParam(value = "file", required = false) MultipartFile file) 
            throws IOException, URISyntaxException {
    JSONObject successJson = new JSONObject();
    if (!file.isEmpty()) {
      String fileName = file.getOriginalFilename();
      String path = null;
      String type = null;
      type = fileName.indexOf(".") 
          != -1 ? fileName.substring(fileName.lastIndexOf(".") 
              + 1, fileName.length()) : null;
      if (type != null) {
        if ("GIF".equals(type.toUpperCase()) 
            || "PNG".equals(type.toUpperCase())  
            || "JPG".equals(type.toUpperCase())) {
          String realPath = request.getSession().getServletContext()
              .getRealPath("/");
          String trueFileName = String.valueOf(System.currentTimeMillis()) 
              + fileName;
          path = realPath + "" + trueFileName;
          String uid = request.getParameter("uid");
          String userType = request.getParameter("type");
          String typeString = userInfoSvc.usertype(uid);
          if (typeString.equals("student") && userType.equals("1")) {
            file.transferTo(new File(path));
            String prize = request.getParameter("prize");
            authenticationSvc.studentAuthentication(uid, prize, trueFileName);
            successJson.put("success",1);
          } else if ((typeString.equals("institutionTeacher") 
              || typeString.equals("schoolTeacher")) && userType.equals("2")) {
            file.transferTo(new File(path));
            successJson.put("userType",userType);
            successJson.put("typeString",typeString);
            String institution = request.getParameter("institution");
            authenticationSvc.teacherAuthentication(uid, institution, 
                trueFileName);
            successJson.put("success",1);
          } else {
            successJson.put("success",0);
          }
        } else {
          successJson.put("success",0);
        }
      } else {
        successJson.put("success",0);
      }
    } else {
      successJson.put("success",0);
    }
    String successString = JSON.toJSONString(successJson);
    Writer out = response.getWriter(); 
    out.write(successString);
    out.flush();
  } 
}
