package com.hgd.iocommunity.controller;

import com.hgd.iocommunity.domain.Other;
import com.hgd.iocommunity.service.ListOtherService;
import com.hgd.iocommunity.state.PageQueryState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ListOtherController {
  @Resource
  private ListOtherService listOtherSvc;
  
  @RequestMapping(value = "ListOtherCtrl", method = RequestMethod.GET)
  private String listOther(Model model, HttpSession session, String page) 
      throws ServletException {
    PageQueryState state = null;
    if (page == null) {
      page = "0";
      session.removeAttribute("PageQueryState");
      state = new PageQueryState();
    } else {
      state = (PageQueryState)session.getAttribute("PageQueryState");
      if (state == null) {
        state = new PageQueryState();
      }
    }
    
    List<Other> list = null;
    try {
      int lastPage = listOtherSvc.getLastPage(state);
      state.setLastPage(lastPage);
      
      list = listOtherSvc.getOtherByPage(state, page);
      session.setAttribute("PageQueryState", state);
      model.addAttribute("lastPage", lastPage);
    } catch (Exception e) {
      e.printStackTrace();
      list = new ArrayList<Other>();
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "发生非预期错误，请联系管理员");
      model.addAttribute("errMap", errMap);
    }
    
    model.addAttribute("listOther", list); 
    return "admin/ManageOther";
  }
  
  @RequestMapping(value = "ListOtherAdminCtrl", method = RequestMethod.GET)
  private String listOtherAdmin(Model model, HttpSession session, 
      String page) throws ServletException {
    PageQueryState state = null;
    if (page == null) {
      page = "0";
      session.removeAttribute("PageQueryState");
      state = new PageQueryState();
    } else {
      state = (PageQueryState)session.getAttribute("PageQueryState");
      if (state == null) {
        state = new PageQueryState();
      }
    }
    
    List<Other> list = null;
    try {
      int lastPage = listOtherSvc.getLastPage(state);
      state.setLastPage(lastPage);
      
      list = listOtherSvc.getOtherByPage(state, page);
      session.setAttribute("PageQueryState", state);
      model.addAttribute("lastPage", lastPage);
    } catch (Exception e) {
      e.printStackTrace();
      list = new ArrayList<Other>();
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "发生非预期错误，请联系管理员");
      model.addAttribute("errMap", errMap);
    }
    
    model.addAttribute("listOther", list); 
    return "admin/AddManagerOther";
  }
}
