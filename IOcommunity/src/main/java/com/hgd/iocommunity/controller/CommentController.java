package com.hgd.iocommunity.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import com.hgd.iocommunity.domain.Comment;
import com.hgd.iocommunity.domain.CommentView;
import com.hgd.iocommunity.service.CommentService;
import com.hgd.iocommunity.service.PostService;
import com.hgd.iocommunity.service.UserInfoService;
import com.hgd.iocommunity.state.CommentQueryState;

import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CommentController {
  @Resource
  private CommentService commentService;
  @Resource
  private CommentService commentViewService;
  @Resource
  private UserInfoService userInfoService;
  @Resource
  private PostService postService;
  
  @RequestMapping(value = "DeleteCommentCtrl")
  public String deleteComment(String postid, int floor) {
    Map<String, Object> map = new HashMap<>();
    map.put("postid", postid);
    map.put("floor", floor);
    commentService.deleteComment(map);
    return "redirect:ListCommentsCtrl";
  }
  
  @RequestMapping(value = "AddCommentCtrl", method = RequestMethod.POST)
  public String addComment(@Valid @ModelAttribute("comment")Comment comment, 
      Errors errors) {
    Comment theComment = new Comment();
    theComment.setFloor(commentService.getMaxFloor(comment.getPostid()) + 1);
    theComment.setPostid(comment.getPostid());
    theComment.setRelate(comment.getRelate());
    theComment.setContent(comment.getContent());
    theComment.setUid(comment.getUid());
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    theComment.setDate(df.format(new Date()));
    theComment.setReply(0);
    theComment.setThumbUp(0);
    theComment.setThumbDown(0);
    if (errors.hasFieldErrors()) {
      return "admin/AddComment";
    }
    try {
      commentService.addComment(theComment);
    } catch (Exception e) {
      e.printStackTrace();
      errors.reject("", "发生非预期错误，请联系管理员");
      return "admin/AddComment";
    }
    return "admin/AddCommentSucc";
  }
  
  @RequestMapping(value = "sendCommentCtrl", method = RequestMethod.POST)
  public void sendComment(HttpServletRequest request, HttpServletResponse 
      response) throws IOException {
    String postid = request.getParameter("postid");
    String relateStr = request.getParameter("relate");
    String uid = request.getParameter("uid");   
    String nickname = null;
    String avatar = null;
    if (userInfoService.getOtherInfo(uid) != null) {
      nickname = userInfoService.getOtherInfo(uid).getNickname();
      avatar = userInfoService.getOtherInfo(uid).getIcon();
    } else if (userInfoService.getParentInfo(uid) != null) {
      nickname = userInfoService.getParentInfo(uid).getNickname();
      avatar = userInfoService.getParentInfo(uid).getIcon();
    } else if (userInfoService.getStudentInfo(uid) != null) {
      nickname = userInfoService.getStudentInfo(uid).getNickname();
      avatar = userInfoService.getStudentInfo(uid).getIcon();
    } else if (userInfoService.getTeacherInfo(uid) != null) {
      nickname = userInfoService.getTeacherInfo(uid).getNickname();
      avatar = userInfoService.getTeacherInfo(uid).getIcon();
    }
    int relate = Integer.parseInt(relateStr);
    String content = request.getParameter("content");  
    int floor = commentService.getMaxFloor(postid) + 1;
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Comment theComment = new Comment(floor, postid, relate, content, uid, 
        df.format(new Date()), 0, 0, 0);
    String date = df.format(new Date()).substring(0, 10);
    int comment = commentService.addComment(theComment);    
    if (comment == 0) {
      JSONObject jsonObject = new JSONObject();
      jsonObject.put("success", 0);   
      Writer out = response.getWriter();
      out.write(jsonObject.toJSONString());
      out.flush();
    } else {
      postService.updateComment(postid);
      JSONObject jsonObject = new JSONObject();
      jsonObject.put("success", 1);
      jsonObject.put("nickname", nickname);
      jsonObject.put("avatar", avatar);
      jsonObject.put("floor", floor);
      jsonObject.put("relate", relate);
      jsonObject.put("content", content);
      jsonObject.put("date", date);
      jsonObject.put("reply", 0);
      jsonObject.put("thumbUp", 0);
      jsonObject.put("thumbDown", 0);
      Writer out;
      out = response.getWriter();
      out.write(jsonObject.toJSONString());
      out.flush();
    }
  }
  
  @RequestMapping(value = "ListCommentsCtrl", method = RequestMethod.GET)
  public String listAll(Model model, HttpSession session, String page, String postid) {
    CommentQueryState state = null;
    if (page == null) {
      page = "0";
      session.removeAttribute("CommentQueryState");
      state = new CommentQueryState(0, postid);
    } else {
      state = (CommentQueryState)session.getAttribute("CommentQueryState");
      if (state == null) {
        state = new CommentQueryState(0, postid);
      }
    }
    
    List<CommentView> list = null;
    try {
      int lastPage = commentViewService.getLastPageByPostid(state);
      state.setLastPage(lastPage);
      
      list = commentViewService.getCommentsByPageAndPostid(state, page);
      session.setAttribute("CommentQueryState", state);
      model.addAttribute("lastPage", lastPage);
    } catch (Exception e) {
      e.printStackTrace();
      list = new ArrayList<CommentView>();
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "发生非预期错误，请联系管理员");
      model.addAttribute("errMap", errMap);
    }
    
    model.addAttribute("listComments", list); 
    return "admin/CommentManage";
  }
  
  @RequestMapping(value = "ListCommentsModularCtrl", method = RequestMethod.GET)
  public String listAllModular(Model model, HttpSession session, String page, String postid) {
    CommentQueryState state = null;
    if (page == null) {
      page = "0";
      session.removeAttribute("CommentQueryState");
      state = new CommentQueryState(0, postid);
    } else {
      state = (CommentQueryState)session.getAttribute("CommentQueryState");
      if (state == null) {
        state = new CommentQueryState(0, postid);
      }
    }
    
    List<CommentView> list = null;
    try {
      int lastPage = commentViewService.getLastPageByPostid(state);
      state.setLastPage(lastPage);
      
      list = commentViewService.getCommentsByPageAndPostid(state, page);
      session.setAttribute("CommentQueryState", state);
      model.addAttribute("lastPage", lastPage);
    } catch (Exception e) {
      e.printStackTrace();
      list = new ArrayList<CommentView>();
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "发生非预期错误，请联系管理员");
      model.addAttribute("errMap", errMap);
    }
    
    model.addAttribute("listComments", list); 
    return "admin/CommentManageModular";
  }
  
  @RequestMapping(value = "getCommentsCtrl", method = RequestMethod.POST)
  public void getComments(HttpServletRequest request, HttpServletResponse 
      response) throws IOException {
    String postid = request.getParameter("postid");
    String pageStr = request.getParameter("page");
    int page = Integer.parseInt(pageStr);
    CommentQueryState state = new CommentQueryState(page, postid);
    List<CommentView> list = commentViewService.getComments(state);
    JSONArray jsonarray = new JSONArray();
    for (int i = 0; i < list.size(); i++) {
      CommentView comment = list.get(i);
      String date = comment.getDate().substring(0, 10);
      JSONObject jsonobject = new JSONObject();     
      jsonobject.put("nickname", comment.getNickname());
      jsonobject.put("avatar", comment.getIcon());
      jsonobject.put("floor", comment.getFloor());
      jsonobject.put("relate", comment.getRelate());
      jsonobject.put("content", comment.getContent());
      jsonobject.put("date", date);
      jsonobject.put("reply", comment.getReply());
      jsonobject.put("thumbUp", comment.getThumbUp());
      jsonobject.put("thumbDown", comment.getThumbDown());
      jsonarray.add(jsonobject);
    }
    Writer out = response.getWriter(); 
    out.write(jsonarray.toJSONString());
    out.flush();
  }
  
  @RequestMapping(value = "isLikeCommentCtrl", method = RequestMethod.POST)
  public void isLikeComment(HttpServletRequest request, HttpServletResponse 
      response) {
    String postid = request.getParameter("id");
    String floorStr = request.getParameter("floor");
    String likeStr = request.getParameter("like");
    String unlikeStr = request.getParameter("unlike");
    int floor = Integer.parseInt(floorStr);    
    Map<String, Object> map = new HashMap<>();
    map.put("floor", floor);
    map.put("postid", postid);
    int like = Integer.parseInt(likeStr);
    int unlike = Integer.parseInt(unlikeStr);
    if (like == 0) {
      commentService.like(map);
    }
    if (unlike == 0) {
      commentService.unlike(map);
    }
  }
}