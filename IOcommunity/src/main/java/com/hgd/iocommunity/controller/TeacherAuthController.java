package com.hgd.iocommunity.controller;

import com.hgd.iocommunity.domain.Teacher;
import com.hgd.iocommunity.domain.TeacherAuth;
import com.hgd.iocommunity.service.TeacherAuthService;
import com.hgd.iocommunity.service.UserInfoService;
import com.hgd.iocommunity.util.SendSms;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class TeacherAuthController {
  @Resource
  private TeacherAuthService teacherAuthServ;
  
  @Resource
  private UserInfoService userInfoSvc;
  
  @RequestMapping(value = "/ListTeacherAuthCtrl", method 
      = RequestMethod.GET)
  public String listAll(Model model, HttpSession session) {
    List<TeacherAuth> list = null;
    try {
      list = teacherAuthServ.getAuthList();
      model.addAttribute("TeacherAuthList", list);
    } catch (Exception e) {
      e.printStackTrace();
    }
    
    return "admin/TeacherAuth";
  }
  
  @RequestMapping(value = "/deleteTeacherAuthCtrl", method 
      = RequestMethod.GET)
  public String delete(String tid) 
      throws IOException, URISyntaxException {
    Teacher teacher = userInfoSvc.getTeacherInfo(tid);
    String text = "【信奥社区】对不起！您的认证未通过，请确认认证信息之后重新提交";
    new SendSms().sendsms(teacher.getPhonenumber(),text);
    teacherAuthServ.deleteAuthOfTeachert(tid);
    return "redirect:/ListTeacherAuthCtrl";
  }
  
  @RequestMapping(value = "/successTeacherAuthCtrl", method 
      = RequestMethod.GET)
  public String success(String tid,String institution) 
      throws IOException, URISyntaxException {
    System.out.println(tid);
    System.out.println(institution);
    teacherAuthServ.success(tid, institution);
    Teacher teacher = userInfoSvc.getTeacherInfo(tid);
    String text = "【信奥社区】您的认证已通过，请登录小程序查看！";
    new SendSms().sendsms(teacher.getPhonenumber(),text);
    return "redirect:/ListTeacherAuthCtrl";
  }
}
