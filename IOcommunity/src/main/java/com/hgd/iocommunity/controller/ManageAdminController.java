package com.hgd.iocommunity.controller;

import com.hgd.iocommunity.service.LoginService;
import com.hgd.iocommunity.service.UserInfoService;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ManageAdminController {
  @Resource
  private UserInfoService userInfoSvc;
  
  @Resource
  private LoginService loginSvc;
  
  @RequestMapping(value = "AddAdminStuCtrl", method = RequestMethod.GET)
  private String addAdminStudent(Model model, HttpSession session, 
      String phonenumber, String province) {
    String adminType = loginSvc.admintype(phonenumber);
    if (adminType.equals("user") && phonenumber.length() == 11) {
      userInfoSvc.addAdmin(phonenumber,province);
    } else if(adminType.equals("modularAdmin") || adminType.equals("superAdmin")) {
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "该号码已被管理员绑定");
      model.addAttribute("errMap", errMap);
    } else {
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "请先绑定电话号码");
      model.addAttribute("errMap", errMap);
    }
    return "redirect:ListStudentsAdminCtrl";
  }
  
  @RequestMapping(value = "AddAdminStuCtrl", method = RequestMethod.POST)
  private String addNAdminStudent(Model model, HttpSession session, 
      String phonenumber) {
    String adminType = loginSvc.admintype(phonenumber);
    if (adminType.equals("user") && phonenumber.length() == 11) {
      userInfoSvc.addAdmin(phonenumber,"全国");
    } else if(adminType.equals("modularAdmin") || adminType.equals("superAdmin")) {
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "该号码已被管理员绑定");
      model.addAttribute("errMap", errMap);
    } else {
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "请输入正确的电话号码");
      model.addAttribute("errMap", errMap);
    }
    return "redirect:ListStudentsAdminCtrl";
  }
  
  @RequestMapping(value = "AddAdminTeaCtrl", method = RequestMethod.GET)
  private String addAdminTeacher(Model model, HttpSession session, 
      String phonenumber, String province) {
    String adminType = loginSvc.admintype(phonenumber);
    if (adminType.equals("user") && phonenumber.length() == 11) {
      userInfoSvc.addAdmin(phonenumber,province);
    } else if(adminType.equals("modularAdmin") || adminType.equals("superAdmin")) {
      Map<String, String> errAdmin = new HashMap<String, String>();
      errAdmin.put("GLOBAL", "该号码已被管理员绑定");
      model.addAttribute("errAdmin", errAdmin);
    } else {
      Map<String, String> errAdmin = new HashMap<String, String>();
      errAdmin.put("GLOBAL", "请先绑定电话号码");
      model.addAttribute("errAdmin", errAdmin);
    }
    return "redirect:ListTeacherAdminCtrl";
  }
  
  @RequestMapping(value = "AddAdminTeaCtrl", method = RequestMethod.POST)
  private String addNAdminTeacher(Model model, HttpSession session, 
      String phonenumber) {
    String adminType = loginSvc.admintype(phonenumber);
    if (adminType.equals("user") && phonenumber.length() == 11) {
      userInfoSvc.addAdmin(phonenumber,"全国");
    } else if(adminType.equals("modularAdmin") || adminType.equals("superAdmin")) {
      Map<String, String> errAdmin = new HashMap<String, String>();
      errAdmin.put("GLOBAL", "该号码已被管理员绑定");
      model.addAttribute("errAdmin", errAdmin);
    } else {
      Map<String, String> errAdmin = new HashMap<String, String>();
      errAdmin.put("GLOBAL", "请输入正确的电话号码");
      model.addAttribute("errAdmin", errAdmin);
    }
    return "redirect:ListTeacherAdminCtrl";
  }
  
  @RequestMapping(value = "AddAdminParCtrl", method = RequestMethod.GET)
  private String addAdminParent(Model model, HttpSession session, 
      String phonenumber, String province) {
    String adminType = loginSvc.admintype(phonenumber);
    if (adminType.equals("user") && phonenumber.length() == 11) {
      userInfoSvc.addAdmin(phonenumber,province);
    } else if(adminType.equals("modularAdmin") || adminType.equals("superAdmin")) {
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "该号码已被管理员绑定");
      model.addAttribute("errMap", errMap);
    } else {
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "请先绑定电话号码");
      model.addAttribute("errMap", errMap);
    }
    return "redirect:ListParentAdminCtrl";
  }
  
  @RequestMapping(value = "AddAdminParCtrl", method = RequestMethod.POST)
  private String addNAdminParent(Model model, HttpSession session, 
      String phonenumber) {
    String adminType = loginSvc.admintype(phonenumber);
    if (adminType.equals("user") && phonenumber.length() == 11) {
      userInfoSvc.addAdmin(phonenumber,"全国");
    } else if(adminType.equals("modularAdmin") || adminType.equals("superAdmin")) {
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "该号码已被管理员绑定");
      model.addAttribute("errMap", errMap);
    } else {
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "请输入正确的电话号码");
      model.addAttribute("errMap", errMap);
    }
    return "redirect:ListParentAdminCtrl";
  }
  
  @RequestMapping(value = "AddAdminOthCtrl", method = RequestMethod.GET)
  private String addAdminOther(Model model, HttpSession session, 
      String phonenumber, String province) {
    String adminType = loginSvc.admintype(phonenumber);
    if (adminType.equals("user") && phonenumber.length() == 11) {
      userInfoSvc.addAdmin(phonenumber,province);
    } else if(adminType.equals("modularAdmin") || adminType.equals("superAdmin")) {
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "该号码已被管理员绑定");
      model.addAttribute("errMap", errMap);
    } else {
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "请先绑定电话号码");
      model.addAttribute("errMap", errMap);
    }
    return "redirect:ListOtherAdminCtrl";
  }
  
  @RequestMapping(value = "AddAdminOthCtrl", method = RequestMethod.POST)
  private String addNAdminOther(Model model, HttpSession session, 
      String phonenumber) {
    String adminType = loginSvc.admintype(phonenumber);
    if (adminType.equals("user") && phonenumber.length() == 11) {
      userInfoSvc.addAdmin(phonenumber,"全国");
    } else if(adminType.equals("modularAdmin") || adminType.equals("superAdmin")) {
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "该号码已被管理员绑定");
      model.addAttribute("errMap", errMap);
    } else {
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "请输入正确的电话号码");
      model.addAttribute("errMap", errMap);
    }
    return "redirect:ListOtherAdminCtrl";
  }
}
