package com.hgd.iocommunity.controller;

import com.hgd.iocommunity.domain.Teacher;
import com.hgd.iocommunity.service.ListTeacherService;
import com.hgd.iocommunity.state.PageQueryState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ListTeacherController {
  @Resource
  private ListTeacherService listTeacherSvc;
  
  @RequestMapping(value = "ListTeacherCtrl", method = RequestMethod.GET)
  private String listTeacher(Model model, HttpSession session, String page) 
      throws ServletException {
    PageQueryState state = null;
    if (page == null) {
      page = "0";
      session.removeAttribute("PageQueryState");
      state = new PageQueryState();
    } else {
      state = (PageQueryState)session.getAttribute("PageQueryState");
      if (state == null) {
        state = new PageQueryState();
      }
    }
    
    List<Teacher> list = null;
    try {
      int lastPage = listTeacherSvc.getLastPage(state);
      state.setLastPage(lastPage);
      
      list = listTeacherSvc.getTeacherByPage(state, page);
      session.setAttribute("PageQueryState", state);
      model.addAttribute("lastPage", lastPage);
    } catch (Exception e) {
      e.printStackTrace();
      list = new ArrayList<Teacher>();
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "发生非预期错误，请联系管理员");
      model.addAttribute("errMap", errMap);
    }
    
    model.addAttribute("listTeacher", list); 
    return "admin/ManageTeacher";
  }
  
  @RequestMapping(value = "ListTeacherAdminCtrl", method = RequestMethod.GET)
  private String listTeacherAdmin(Model model, HttpSession session, 
      String page) throws ServletException {
    PageQueryState state = null;
    if (page == null) {
      page = "0";
      session.removeAttribute("PageQueryState");
      state = new PageQueryState();
    } else {
      state = (PageQueryState)session.getAttribute("PageQueryState");
      if (state == null) {
        state = new PageQueryState();
      }
    }
    
    List<Teacher> list = null;
    try {
      int lastPage = listTeacherSvc.getLastPage(state);
      state.setLastPage(lastPage);
      
      list = listTeacherSvc.getTeacherByPage(state, page);
      session.setAttribute("PageQueryState", state);
      model.addAttribute("lastPage", lastPage);
    } catch (Exception e) {
      e.printStackTrace();
      list = new ArrayList<Teacher>();
      Map<String, String> errMap = new HashMap<String, String>();
      errMap.put("GLOBAL", "发生非预期错误，请联系管理员");
      model.addAttribute("errMap", errMap);
    }
    
    model.addAttribute("listTeacher", list); 
    return "admin/AddManagerTeacher";
  }
}
