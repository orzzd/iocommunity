package com.hgd.iocommunity.controller;

import com.alibaba.fastjson.JSONObject;
import com.hgd.iocommunity.domain.CommentLikeState;
import com.hgd.iocommunity.domain.LikeState;
import com.hgd.iocommunity.service.CommentLikeStateService;
import com.hgd.iocommunity.service.LikeStateService;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LikeStateController {
  @Resource
  private LikeStateService likeStateService;
  @Resource
  private CommentLikeStateService commentLikeStateService;
  
  @RequestMapping(value = "addLikeStateCtrl", method = RequestMethod.POST)
  public void addLikeState(HttpServletRequest request, HttpServletResponse 
      response) {
    String postid = request.getParameter("postid");
    String uid = request.getParameter("uid");
    String likeStr = request.getParameter("like");
    String unlikeStr = request.getParameter("unlike");
    int like = Integer.parseInt(likeStr);
    int unlike = Integer.parseInt(unlikeStr);
    LikeState likeState = new LikeState(uid, postid, like, unlike);
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("like", like);
    map.put("unlike", unlike);
    map.put("postid", postid);
    map.put("uid", uid);
    try {
      LikeState state = likeStateService.selectLikeState(postid, uid);
      if (state == null) {        
        likeStateService.addLikeState(likeState);
      } else {
        likeStateService.like(map);
        likeStateService.unlike(map);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @RequestMapping(value = "sendLikeStateCtrl", method = RequestMethod.POST)
  public void sendLikeState(HttpServletRequest request, HttpServletResponse 
      response) throws IOException {
    String postid = request.getParameter("postid");
    String uid = request.getParameter("uid");
    
    LikeState state = likeStateService.selectLikeState(postid, uid);
    int successNum;
    if (state != null) {
      successNum = 1;
      JSONObject jsonObject = new JSONObject();
      jsonObject.put("success", successNum);
      jsonObject.put("uid", state.getUid());
      jsonObject.put("postid", state.getPostid());
      jsonObject.put("like", state.getLike());
      jsonObject.put("unlike", state.getUnlike());
      Writer out = response.getWriter();
      out.write(jsonObject.toJSONString());
      out.flush();
    } else {
      successNum = 0;
      JSONObject jsonObject = new JSONObject();
      jsonObject.put("success", successNum);
      Writer out = response.getWriter();
      out.write(jsonObject.toJSONString());
      out.flush();
    }
  }
  
  @RequestMapping(value = "addCommentLikeStateCtrl", method 
      = RequestMethod.POST)
  public void addCommentLikeState(HttpServletRequest request, 
      HttpServletResponse response) {
    String postid = request.getParameter("postid");
    String uid = request.getParameter("uid");
    String likeStr = request.getParameter("like");
    String unlikeStr = request.getParameter("unlike");
    String floorStr = request.getParameter("floor");
    int like = Integer.parseInt(likeStr);
    int unlike = Integer.parseInt(unlikeStr);
    int floor = Integer.parseInt(floorStr);
    CommentLikeState commentLikeState = new CommentLikeState(uid, postid, 
        floor, like, unlike);
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("like", like);
    map.put("unlike", unlike);
    map.put("postid", postid);
    map.put("uid", uid);
    map.put("floor", floor);
    try {
      CommentLikeState state = commentLikeStateService.selectCommentLikeState(
          postid, uid, floor);
      if (state == null) {        
        commentLikeStateService.insert(commentLikeState);
      } else {
        commentLikeStateService.like(map);
        commentLikeStateService.unlike(map);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @RequestMapping(value = "sendCommentLikeStateCtrl", method = 
      RequestMethod.POST)
  public void sendCommentLikeState(HttpServletRequest request, 
      HttpServletResponse response) throws IOException {
    String postid = request.getParameter("postid");
    String uid = request.getParameter("uid");
    String floorStr = request.getParameter("floor");
    int floor = Integer.parseInt(floorStr);
    
    CommentLikeState state = commentLikeStateService.selectCommentLikeState(
        postid, uid, floor);
    
    if (state != null) {
      JSONObject jsonObject = new JSONObject();
      jsonObject.put("success", 1);
      jsonObject.put("uid", state.getUid());
      jsonObject.put("postid", state.getPostid());
      jsonObject.put("like", state.getLike());
      jsonObject.put("unlike", state.getUnlike());
      jsonObject.put("floor", state.getFloor());
      Writer out = response.getWriter();
      out.write(jsonObject.toJSONString());
      out.flush();
    } else {
      JSONObject jsonObject = new JSONObject();
      jsonObject.put("success", 0);
      Writer out = response.getWriter();
      out.write(jsonObject.toJSONString());
      out.flush();
    }
  }
}
