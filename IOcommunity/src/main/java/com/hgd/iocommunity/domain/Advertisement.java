package com.hgd.iocommunity.domain;

public class Advertisement {
  
  private int adid;
  private String vid;
  private String title;
  
  public Advertisement(int adid, String vid, String title) {
    this.adid = adid;
    this.vid = vid;
    this.title = title;
  }
  
  public Advertisement() {}
  
  public int getAdid() {
    return adid;
  }
  
  public void setAdid(int adid) {
    this.adid = adid;
  }
  
  public String getTitle() {
    return title;
  }
  
  public void setTitle(String title) {
    this.title = title;
  }

  public String getVid() {
    return vid;
  }

  public void setVid(String vid) {
    this.vid = vid;
  }
}
