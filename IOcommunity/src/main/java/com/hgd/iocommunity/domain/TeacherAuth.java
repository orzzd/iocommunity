package com.hgd.iocommunity.domain;

import com.alibaba.fastjson.annotation.JSONField;

public class TeacherAuth {
  @JSONField(name = "tid")
  private String tid;
  
  @JSONField(name = "institution")
  private String institution;
  
  @JSONField(name = "picture")
  private String picture;

  public String getTid() {
    return tid;
  }

  public void setTid(String tid) {
    this.tid = tid;
  }

  public String getInstitution() {
    return institution;
  }

  public void setInstitution(String institution) {
    this.institution = institution;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }
}
