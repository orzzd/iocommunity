package com.hgd.iocommunity.domain;

import com.alibaba.fastjson.annotation.JSONField;

public class Teacher {
  @JSONField(name = "tid")
  private String tid;
  
  @JSONField(name = "nickname")
  private String nickname;
  
  @JSONField(name = "phonenumber")
  private String phonenumber;
  
  @JSONField(name = "province")
  private String province;
  
  @JSONField(name = "institution")
  private String institution;  
  
  @JSONField(name = "ban")
  private int ban;  
  
  @JSONField(name = "icon")
  private String icon;  
  
  @JSONField(name = "teachertype")
  private String teachertype;  
  
  public Teacher(){}
  
  public Teacher(String tid, String nickname, String phonenumber,
      String province, String institution, int ban, String icon, 
      String teachertype) {
    super();
    this.tid = tid;
    this.nickname = nickname;
    this.phonenumber = phonenumber;
    this.province = province;
    this.institution = institution;
    this.ban = ban;
    this.icon = icon;
    this.teachertype = teachertype;
  }
  
  public String getTid() {
    return tid;
  }

  public void setTid(String tid) {
    this.tid = tid;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public String getPhonenumber() {
    return phonenumber;
  }

  public void setPhonenumber(String phonenumber) {
    this.phonenumber = phonenumber;
  }

  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public String getInstitution() {
    return institution;
  }

  public void setInstitution(String institution) {
    this.institution = institution;
  }

  public int getBan() {
    return ban;
  }

  public void setBan(int ban) {
    this.ban = ban;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public String getTeachertype() {
    return teachertype;
  }

  public void setTeachertype(String teachertype) {
    this.teachertype = teachertype;
  }

  @Override
  public String toString() {
    return "Teacher [tid=" + tid + ", nickname=" + nickname 
        + ", phonenumber=" + phonenumber + ", province=" + province 
        + ", institution=" + institution + ", ban=" + ban + ", icon="
        + icon + ", teachertype=" + teachertype + "]";
  }

}
