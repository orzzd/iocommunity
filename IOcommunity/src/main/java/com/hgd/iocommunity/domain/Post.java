package com.hgd.iocommunity.domain;

import com.alibaba.fastjson.annotation.JSONField;

import org.hibernate.validator.constraints.NotEmpty;

public class Post {
  @JSONField(name = "postid")
  private String postid;
  
  @NotEmpty(message = "标题不能为空") 
  @JSONField(name = "title")
  private String title;
  
  @NotEmpty(message = "发帖人ID不能为空")
  @JSONField(name = "uid")
  private String uid;
  
  @NotEmpty(message = "范围不能为空")
  @JSONField(name = "range")
  private String range;
  
  @NotEmpty(message = "内容不能为空")
  @JSONField(name = "content")
  private String content;
  
  @JSONField(name = "commentAllowed")
  private int commentAllowed;
  
  @JSONField(name = "isEssence")
  private int isEssence;
  
  @JSONField(name = "isFold")
  private int isFold;
  
  @JSONField(name = "date")
  private String date;
  
  @JSONField(name = "comment")
  private int comment;
  
  @JSONField(name = "thumbUp")
  private int thumbUp;
  
  @JSONField(name = "thumbDown")
  private int thumbDown;
  
  @JSONField(name = "clickRate")
  private int clickRate;
  
  public Post() {}
  
  public Post(String postid, String title, String uid, String range, 
      String content, int commentAllowed, int isEssence, int isFold, 
      String date, int comment, int thumbUp, int thumbDown, int clickRate) {
    this.postid = postid;
    this.title = title;
    this.uid = uid;
    this.range = range;
    this.content = content;
    this.commentAllowed = commentAllowed;
    this.isEssence = isEssence;
    this.isFold = isFold;
    this.date = date;
    this.comment = comment;
    this.thumbUp = thumbUp;
    this.thumbDown = thumbDown;
    this.clickRate = clickRate;
  }
  
  public String getPostid() {
    return postid;
  }
  
  public void setPostid(String postid) {
    this.postid = postid;
  }
  
  public String getTitle() {
    return title;
  }
  
  public void setTitle(String title) {
    this.title = title;
  }
  
  public String getUid() {
    return uid;
  }
  
  public void setUid(String uid) {
    this.uid = uid;
  }
  
  public String getRange() {
    return range;
  }
  
  public void setRange(String range) {
    this.range = range;
  }
  
  public String getContent() {
    return content;
  }
  
  public void setContent(String content) {
    this.content = content;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public int getComment() {
    return comment;
  }

  public void setComment(int comment) {
    this.comment = comment;
  }

  public int getCommentAllowed() {
    return commentAllowed;
  }

  public void setCommentAllowed(int commentAllowed) {
    this.commentAllowed = commentAllowed;
  }

  public int getIsEssence() {
    return isEssence;
  }

  public void setIsEssence(int isEssence) {
    this.isEssence = isEssence;
  }

  public int getIsFold() {
    return isFold;
  }

  public void setIsFold(int isFold) {
    this.isFold = isFold;
  }

  public int getThumbUp() {
    return thumbUp;
  }

  public void setThumbUp(int thumbUp) {
    this.thumbUp = thumbUp;
  }

  public int getThumbDown() {
    return thumbDown;
  }

  public void setThumbDown(int thumbDown) {
    this.thumbDown = thumbDown;
  }

  public int getClickRate() {
    return clickRate;
  }

  public void setClickRate(int clickRate) {
    this.clickRate = clickRate;
  }
}