package com.hgd.iocommunity.domain;

import com.alibaba.fastjson.annotation.JSONField;

public class Other {
  @JSONField(name = "oid")
  private String oid;
  
  @JSONField(name = "nickname")
  private String nickname;
  
  @JSONField(name = "phonenumber")
  private String phonenumber;
  
  @JSONField(name = "province")
  private String province;
  
  @JSONField(name = "ban")
  private int ban;
  
  @JSONField(name = "icon")
  private String icon;


  public Other() {}

  public Other(String oid, String nickname, String phonenumber, 
      String province, int ban, String icon) {
    super();
    this.oid = oid;
    this.nickname = nickname;
    this.phonenumber = phonenumber;
    this.province = province;
    this.ban = ban;
    this.icon = icon;
  }

  public String getOid() {
    return oid;
  }

  public void setOid(String oid) {
    this.oid = oid;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public String getPhonenumber() {
    return phonenumber;
  }

  public void setPhonenumber(String phonenumber) {
    this.phonenumber = phonenumber;
  }

  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }
  
  public int getBan() {
    return ban;
  }

  public void setBan(int ban) {
    this.ban = ban;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  @Override
  public String toString() {
    return "Other [oid=" + oid + ", nickname=" + nickname 
        + ", phonenumber=" + phonenumber + ", province=" 
        + province  + ", ban=" + ban + ", icon=" + icon + "]";
  }

}

  
