package com.hgd.iocommunity.domain;

public class LikeState {
  
  private String uid;
  
  private String postid;
  
  private int like;
  
  private int unlike;
  
  public LikeState() {}
  
  public LikeState(String uid, String postid, int like, int unlike) {
    this.uid = uid;
    this.postid = postid;
    this.like = like;
    this.unlike = unlike;
  }
  
  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public String getPostid() {
    return postid;
  }

  public void setPostid(String postid) {
    this.postid = postid;
  }

  public int getLike() {
    return like;
  }

  public void setLike(int like) {
    this.like = like;
  }

  public int getUnlike() {
    return unlike;
  }

  public void setUnlike(int unlike) {
    this.unlike = unlike;
  }
}
