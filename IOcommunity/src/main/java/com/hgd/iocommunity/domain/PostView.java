package com.hgd.iocommunity.domain;

import com.alibaba.fastjson.annotation.JSONField;

public class PostView {
  @JSONField(name = "postid")
  private String postid;
  
  @JSONField(name = "title")
  private String title;
  
  @JSONField(name = "uid")
  private String uid;
  
  @JSONField(name = "range")
  private String range;
  
  @JSONField(name = "content")
  private String content;
  
  @JSONField(name = "commentAllowed")
  private int commentAllowed;
  
  @JSONField(name = "isEssence")
  private int isEssence;
  
  @JSONField(name = "isFold")
  private int isFold;
  
  @JSONField(name = "date")
  private String date;
  
  @JSONField(name = "comment")
  private int comment;
  
  @JSONField(name = "thumbUp")
  private int thumbUp;
  
  @JSONField(name = "thumbDown")
  private int thumbDown;
  
  @JSONField(name = "nickname")
  private String nickname;
  
  @JSONField(name = "icon")
  private String icon;
  
  @JSONField(name = "authentication")
  private String authentication;
  
  @JSONField(name = "clickRate")
  private int clickRate;
  
  public PostView() {}
  
  public PostView(String postid, String title, String uid, String range, 
      String content, int commentAllowed, int isEssence, int isFold, 
      String date, int comment, int thumbUp, int thumbDown, String nickname, 
      String icon, String authentication, int clickRate) {
    this.postid = postid;
    this.title = title;
    this.uid = uid;
    this.range = range;
    this.content = content;
    this.commentAllowed = commentAllowed;
    this.isEssence = isEssence;
    this.isFold = isFold;
    this.date = date;
    this.comment = comment;
    this.thumbUp = thumbUp;
    this.thumbDown = thumbDown;
    this.nickname = nickname;
    this.icon = icon;
    this.authentication = authentication;
    this.clickRate = clickRate;
  }

  public String getPostid() {
    return postid;
  }

  public void setPostid(String postid) {
    this.postid = postid;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public String getRange() {
    return range;
  }

  public void setRange(String range) {
    this.range = range;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public int getCommentAllowed() {
    return commentAllowed;
  }

  public void setCommentAllowed(int commentAllowed) {
    this.commentAllowed = commentAllowed;
  }

  public int getIsEssence() {
    return isEssence;
  }

  public void setIsEssence(int isEssence) {
    this.isEssence = isEssence;
  }

  public int getIsFold() {
    return isFold;
  }

  public void setIsFold(int isFold) {
    this.isFold = isFold;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public int getComment() {
    return comment;
  }

  public void setComment(int comment) {
    this.comment = comment;
  }

  public int getThumbUp() {
    return thumbUp;
  }

  public void setThumbUp(int thumbUp) {
    this.thumbUp = thumbUp;
  }

  public int getThumbDown() {
    return thumbDown;
  }

  public void setThumbDown(int thumbDown) {
    this.thumbDown = thumbDown;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public String getAuthentication() {
    return authentication;
  }

  public void setAuthentication(String authentication) {
    this.authentication = authentication;
  }

  public int getClickRate() {
    return clickRate;
  }

  public void setClickRate(int clickRate) {
    this.clickRate = clickRate;
  }

}
