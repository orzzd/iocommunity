package com.hgd.iocommunity.domain;

import com.alibaba.fastjson.annotation.JSONField;

public class Student {
  @JSONField(name = "sid")
  private String sid;

  @JSONField(name = "province")
  private String province;

  @JSONField(name = "phonenumber")
  private String phonenumber;

  @JSONField(name = "identity")
  private String identity;

  @JSONField(name = "nickname")
  private String nickname;

  @JSONField(name = "school")
  private String school;

  @JSONField(name = "prize")
  private String prize;

  @JSONField(name = "ban")
  private int ban;

  @JSONField(name = "icon")
  private String icon;

  public Student() {}
  
  public Student(String sid, String province, String phonenumber, 
      String identity, String nickname, String school, String prize, 
      int ban,String icon) {
    super();
    this.sid = sid;
    this.province = province;
    this.phonenumber = phonenumber;
    this.identity = identity;
    this.nickname = nickname;
    this.school = school;
    this.prize = prize;
    this.ban = ban;
    this.icon = icon;
  }

  public String getSid() {
    return sid;
  }

  public void setSid(String sid) {
    this.sid = sid;
  }

  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public String getPhonenumber() {
    return phonenumber;
  }

  public void setPhonenumber(String phonenumber) {
    this.phonenumber = phonenumber;
  }

  public String getIdentity() {
    return identity;
  }

  public void setIdentity(String identity) {
    this.identity = identity;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public String getSchool() {
    return school;
  }

  public void setSchool(String school) {
    this.school = school;
  }

  public String getPrize() {
    return prize;
  }

  public void setPrize(String prize) {
    this.prize = prize;
  }

  public int getBan() {
    return ban;
  }

  public void setBan(int ban) {
    this.ban = ban;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }
}