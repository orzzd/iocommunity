package com.hgd.iocommunity.domain;

public class CommentLikeState {
  private String uid;
  private String postid;
  private int floor;
  private int like;
  private int unlike;
  
  public CommentLikeState() {}
  
  public CommentLikeState(String uid, String postid, int floor, 
      int like, int unlike) {
    this.uid = uid;
    this.postid = postid;
    this.floor = floor;
    this.like = like;
    this.unlike = unlike;
  }
  
  public String getUid() {
    return uid;
  }
  
  public void setUid(String uid) {
    this.uid = uid;
  }
  
  public String getPostid() {
    return postid;
  }
  
  public void setPostid(String postid) {
    this.postid = postid;
  }
  
  public int getFloor() {
    return floor;
  }
  
  public void setFloor(int floor) {
    this.floor = floor;
  }
  
  public int getLike() {
    return like;
  }
  
  public void setLike(int like) {
    this.like = like;
  }
  
  public int getUnlike() {
    return unlike;
  }
  
  public void setUnlike(int unlike) {
    this.unlike = unlike;
  }  
}
