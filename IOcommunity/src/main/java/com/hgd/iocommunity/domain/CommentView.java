package com.hgd.iocommunity.domain;

import com.alibaba.fastjson.annotation.JSONField;

public class CommentView {
  @JSONField(name = "floor")
  private int floor;
  
  @JSONField(name = "postid")
  private String postid;
  
  @JSONField(name = "relate")
  private int relate;
  
  @JSONField(name = "content")
  private String content;
  
  @JSONField(name = "uid")
  private String uid; 
  
  @JSONField(name = "date")
  private String date;
  
  @JSONField(name = "reply")
  private int reply;
  
  @JSONField(name = "thumbUp")
  private int thumbUp;
  
  @JSONField(name = "thumbDown")
  private int thumbDown;
  
  @JSONField(name = "nickname")
  private String nickname;
  
  @JSONField(name = "icon")
  private String icon;
  
  public CommentView() {}
  
  public CommentView(int floor, String postid, int relate, String content, 
      String uid, String date, int reply, int thumbUp, int thumbDown, 
      String nickname, String icon) {
    this.setFloor(floor);
    this.setPostid(postid);
    this.setRelate(relate);
    this.setContent(content);
    this.setUid(uid);
    this.setDate(date);
    this.setReply(reply);
    this.setThumbUp(thumbUp);
    this.setThumbDown(thumbDown);
    this.getNickname();
    this.setIcon(icon);
  }

  public int getFloor() {
    return floor;
  }

  public void setFloor(int floor) {
    this.floor = floor;
  }

  public String getPostid() {
    return postid;
  }

  public void setPostid(String postid) {
    this.postid = postid;
  }

  public int getRelate() {
    return relate;
  }

  public void setRelate(int relate) {
    this.relate = relate;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public int getReply() {
    return reply;
  }

  public void setReply(int reply) {
    this.reply = reply;
  }

  public int getThumbUp() {
    return thumbUp;
  }

  public void setThumbUp(int thumbUp) {
    this.thumbUp = thumbUp;
  }

  public int getThumbDown() {
    return thumbDown;
  }

  public void setThumbDown(int thumbDown) {
    this.thumbDown = thumbDown;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }
}
