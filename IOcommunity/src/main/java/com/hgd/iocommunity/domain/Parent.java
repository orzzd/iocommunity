package com.hgd.iocommunity.domain;

import com.alibaba.fastjson.annotation.JSONField;

public class Parent {
  @JSONField(name = "pid")
  private String pid;
  
  @JSONField(name = "nickname")
  private String nickname;
  
  @JSONField(name = "phonenumber")
  private String phonenumber;
  
  @JSONField(name = "province")
  private String province;
  
  @JSONField(name = "objective")
  private String objective;
  
  @JSONField(name = "ban")
  private int ban;
  
  @JSONField(name = "icon")
  private String icon;
  
  
  public Parent(){}
  
  public Parent(String pid, String nickname, String phonenumber,
      String province, String objective, int ban, String icon) {
    super();
    this.pid = pid;
    this.nickname = nickname;
    this.phonenumber = phonenumber;
    this.province = province;
    this.objective = objective;
    this.ban = ban;
    this.icon = icon;
  }
  
  public String getPid() {
    return pid;
  }

  public void setPid(String pid) {
    this.pid = pid;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public String getPhonenumber() {
    return phonenumber;
  }

  public void setPhonenumber(String phonenumber) {
    this.phonenumber = phonenumber;
  }

  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public String getObjective() {
    return objective;
  }

  public void setObjective(String objective) {
    this.objective = objective;
  }
  
  
  public int getBan() {
    return ban;
  }

  public void setBan(int ban) {
    this.ban = ban;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  @Override
  public String toString() {
    return "Parent [pid=" + pid + ", nickname=" + nickname 
        + ", phonenumber=" + phonenumber + ", province=" + province
        + ", objective=" + objective + ", ban=" + ban + ", icon=" + icon + "]";
  }

}
