package com.hgd.iocommunity.domain;

import com.alibaba.fastjson.annotation.JSONField;

import org.hibernate.validator.constraints.NotEmpty;

public class Comment {
  @JSONField(name = "floor")
  private int floor;
  
  @JSONField(name = "postid")
  @NotEmpty(message = "帖子id不能为空")
  private String postid;
  
  @JSONField(name = "relate")
  private int relate;
  
  @JSONField(name = "content")
  @NotEmpty(message = "内容不能为空") 
  private String content;
  
  @JSONField(name = "uid")
  @NotEmpty(message = "用户ID不能为空")
  private String uid; 
  
  @JSONField(name = "date")
  private String date;
  
  @JSONField(name = "reply")
  private int reply;
  
  @JSONField(name = "thumbUp")
  private int thumbUp;
  
  @JSONField(name = "thumbDown")
  private int thumbDown;
  
  public Comment() {}
  
  public Comment(int floor, String postid, int relate, String content, 
      String uid, String date, int reply, int thumbUp, int thumbDown) {
    this.floor = floor;
    this.postid = postid;
    this.relate = relate;
    this.content = content;
    this.uid = uid;
    this.date = date;
  }

  public int getFloor() {
    return floor;
  }

  public void setFloor(int floor) {
    this.floor = floor;
  }

  public String getPostid() {
    return postid;
  }

  public void setPostid(String postid) {
    this.postid = postid;
  }

  public int getRelate() {
    return relate;
  }

  public void setRelate(int relate) {
    this.relate = relate;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public int getReply() {
    return reply;
  }

  public void setReply(int reply) {
    this.reply = reply;
  }

  public int getThumbUp() {
    return thumbUp;
  }

  public void setThumbUp(int thumbUp) {
    this.thumbUp = thumbUp;
  }

  public int getThumbDown() {
    return thumbDown;
  }

  public void setThumbDown(int thumbDown) {
    this.thumbDown = thumbDown;
  }
}
