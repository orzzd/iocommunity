package com.hgd.iocommunity.domain;

import com.alibaba.fastjson.annotation.JSONField;

public class StudentAuth {
  @JSONField(name = "sid")
  private String sid;
  
  @JSONField(name = "prize")
  private String prize;
  
  @JSONField(name = "picture")
  private String picture;
  
  public StudentAuth(String sid, String prize, String picture) {
    super();
    this.picture = picture;
    this.sid = sid;
    this.prize = prize;
  }

  public String getSid() {
    return sid;
  }

  public void setSid(String sid) {
    this.sid = sid;
  }

  public String getPrize() {
    return prize;
  }

  public void setPrize(String prize) {
    this.prize = prize;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }
}
