package com.hgd.iocommunity.state;

public class CommentQueryState extends PageQueryState {
  private String postid;
  
  public CommentQueryState() { }
  
  public CommentQueryState(int curPage, String postid) {
    setCurPage(curPage);
    this.setPostid(postid);
  }

  public String getPostid() {
    return postid;
  }

  public void setPostid(String postid) {
    this.postid = postid;
  }
  
}
