package com.hgd.iocommunity.state;

public class PostListQueryState extends PageQueryState {
  private String range1;
  private String range2;
  
  public PostListQueryState() {}
  
  public PostListQueryState(int curPage, String range1, String range2) {
    setCurPage(curPage);
    this.setRange1(range1);
    this.setRange2(range2);
  }
  
  public String getRange1() {
    return range1;
  }
  
  public void setRange1(String range1) {
    this.range1 = range1;
  }
  
  public String getRange2() {
    return range2;
  }
  
  public void setRange2(String range2) {
    this.range2 = range2;
  }

}
