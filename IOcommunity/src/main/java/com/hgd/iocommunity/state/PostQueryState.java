package com.hgd.iocommunity.state;

public class PostQueryState extends PageQueryState {
  private String type;
  private String sortMethod;
  
  public PostQueryState() { }
  
  public PostQueryState(int curPage, String type, String sortMethod) {
    setCurPage(curPage);
    this.setType(type);
    this.setSortMethod(sortMethod);
  }
  
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getSortMethod() {
    return sortMethod;
  }

  public void setSortMethod(String sortMethod) {
    this.sortMethod = sortMethod;
  }

}
