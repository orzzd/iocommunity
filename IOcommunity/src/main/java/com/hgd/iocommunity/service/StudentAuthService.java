package com.hgd.iocommunity.service;

import com.hgd.iocommunity.dao.StudentAuthMapper;
import com.hgd.iocommunity.domain.StudentAuth;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service("studentAuthService")
@Scope("singleton")
public class StudentAuthService {
  @Resource
  private StudentAuthMapper studentAuthServ;
  @Resource
  private StudentAuthMapper studentAuthDao;
  

  public List<StudentAuth> getAuthList() {
    List<StudentAuth> list = studentAuthDao.queryAll();
    return list;
  }
  
  public void deleteAuth(String picture) {
    studentAuthDao.deleteAuth(picture);
  }
  
  public void deleteAuthOfStudent(String sid) {
    studentAuthDao.deleteAuthOfStudent(sid);
  }
  
  public void success(String sid, String prize) {
    studentAuthDao.deleteAuthOfStudent(sid);
    studentAuthDao.addPrize(prize, sid);
  }
}
