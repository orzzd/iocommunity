package com.hgd.iocommunity.service;

import com.hgd.iocommunity.dao.AuthenticationMapper;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class AuthenticationService {
  @Resource
  private AuthenticationMapper authenticationDao;
  
  public void studentAuthentication(String uid, String prize, String path) {
    Map<String, Object> map = new HashMap<>();
    map.put("uid",uid);
    map.put("prize",prize);
    map.put("picture", path);
    authenticationDao.studentAuthentication(map);
  }
  
  public void teacherAuthentication(String uid, String institution, 
      String path) {
    Map<String, Object> map = new HashMap<>();
    map.put("uid",uid);
    map.put("institution",institution);
    map.put("picture", path);
    authenticationDao.teacherAuthentication(map);
  }
}