package com.hgd.iocommunity.service;

import com.hgd.iocommunity.dao.CommentLikeStateMapper;
import com.hgd.iocommunity.domain.CommentLikeState;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class CommentLikeStateService {
  @Resource
  private CommentLikeStateMapper commentLikeStateDao;
  
  public void insert(CommentLikeState commentLikeState) {
    commentLikeStateDao.insert(commentLikeState);
  }
  
  public CommentLikeState selectCommentLikeState(String postid, 
      String uid, int floor) {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("uid", uid);
    map.put("postid", postid);
    map.put("floor", floor);
    CommentLikeState state = commentLikeStateDao.selectCommentLikeState(map);
    return state;
  }
  
  public void like(Map<String, Object> map) {
    commentLikeStateDao.like(map);
  }
  
  public void unlike(Map<String, Object> map) {
    commentLikeStateDao.unlike(map);
  }
}
