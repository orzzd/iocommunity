package com.hgd.iocommunity.service;

import com.hgd.iocommunity.dao.LoginMapper;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class LoginService {
  @Resource
  private LoginMapper loginDao;
  
  public String admintype(String phonenumber) {
    String admintype = "user";
    if (loginDao.isModularAdmin(phonenumber) == 1) {
      return "modularAdmin";
    } else if (loginDao.isSuperAdmin(phonenumber) == 1) {
      return "superAdmin";
    } 
    return admintype;
  }
}
