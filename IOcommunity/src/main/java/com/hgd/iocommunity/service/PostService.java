package com.hgd.iocommunity.service;

import com.hgd.iocommunity.dao.PostMapper;
import com.hgd.iocommunity.dao.PostViewMapper;
import com.hgd.iocommunity.domain.Post;
import com.hgd.iocommunity.domain.PostView;
import com.hgd.iocommunity.state.PostListQueryState;
import com.hgd.iocommunity.state.PostQueryState;
import com.hgd.iocommunity.util.WebUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class PostService {
  @Resource
  private PostMapper postDao;
  @Resource
  private PostViewMapper postViewDao;
  
  public int addPost(Post post) {
    return postDao.insert(post);
  }
  
  public void deletePost(String postid) {
    postDao.delete(postid);
  }
  
  public int getMaxCount() {
    return postDao.queryMaxCount();
  }
  
  public PostView getPost(String postid) {
    return postViewDao.queryById(postid);
  }
  
  public int getLastPage(PostQueryState state) {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("type", state.getType());
    map.put("sortMethod", state.getSortMethod());
    int count = postViewDao.queryCount(map);
    int maxPage = (count + WebUtil.POST_MAX_PAGE_LINES - 1) 
        / WebUtil.POST_MAX_PAGE_LINES;
    int lastPage = (maxPage > 0) ? maxPage - 1 : 0;
    return lastPage;
  }
  
  public List<PostView> getPostsByPage(PostQueryState state, String page) {
    int curPage = state.getCurPage();
    switch (page) {
      case "0":
        curPage = 0;
        break;
      case "prev":
        if (curPage > 0) {
          curPage--;
        }
        break;
      case "next":
        if (curPage < state.getLastPage()) {
          curPage++;
        }
        break;
      default:
        curPage = state.getLastPage();
        break;
    }
    state.setCurPage(curPage);
    
    Map<String, Object> map = new HashMap<>();
    map.put("startRow", state.getCurPage() * WebUtil.POST_MAX_PAGE_LINES);
    map.put("rowCount", WebUtil.POST_MAX_PAGE_LINES);
    map.put("type", state.getType());
    map.put("sortMethod", state.getSortMethod());
    
    List<PostView> list = postViewDao.queryByPage(map);
    return list;
  }
  
  public List<PostView> getPosts(PostQueryState state) {
    Map<String, Object> map = new HashMap<>();
    map.put("startRow", state.getCurPage() * WebUtil.POST_MAX_PAGE_LINES);
    map.put("rowCount", WebUtil.POST_MAX_PAGE_LINES);
    map.put("type", state.getType());
    map.put("sortMethod", state.getSortMethod());
    
    List<PostView> list = postViewDao.queryByPage(map);
    return list;
  }
  
  public List<PostView> getOtherPosts(PostQueryState state) {
    Map<String, Object> map = new HashMap<>();
    map.put("startRow", state.getCurPage() * WebUtil.POST_MAX_PAGE_LINES);
    map.put("rowCount", WebUtil.POST_MAX_PAGE_LINES);
    map.put("type", state.getType());
    map.put("sortMethod", state.getSortMethod());
    
    List<PostView> list = postViewDao.queryOtherByPage(map);
    return list;
  }
  
  public void fold(String postid) {
    postDao.fold(postid);
  }
  
  public void unfold(String postid) {
    postDao.unfold(postid);
  }
  
  public void digest(String postid) {
    postDao.digest(postid);
  }
  
  public void nodigest(String postid) {
    postDao.nodigest(postid);
  }
  
  public void nocomment(String postid) {
    postDao.nocomment(postid);
  }
  
  public void comment(String postid) {
    postDao.comment(postid);
  }
  
  public void like(String postid) {
    postDao.like(postid);
  }
  
  public void unlike(String postid) {
    postDao.unlike(postid);
  }
  
  public void notLike(String postid) {
    postDao.notLike(postid);
  }
  
  public void notUnlike(String postid) {
    postDao.notUnlike(postid);
  }
  
  public void updateComment(String postid) {
    postDao.updateComment(postid);
  }
    
  public int getLastPageByRange(PostListQueryState state) {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("range1", state.getRange1());
    map.put("range2", state.getRange2());
    int count = postViewDao.queryCountByRange(map);
    int maxPage = (count + WebUtil.POST_MAX_PAGE_LINES - 1) 
        / WebUtil.POST_MAX_PAGE_LINES;
    int lastPage = (maxPage > 0) ? maxPage - 1 : 0;
    return lastPage;
  }
  
  public List<PostView> getPostsByPageAndRange(PostListQueryState state, String page) {
    int curPage = state.getCurPage();
    switch (page) {
      case "0":
        curPage = 0;
        break;
      case "prev":
        if (curPage > 0) {
          curPage--;
        }
        break;
      case "next":
        if (curPage < state.getLastPage()) {
          curPage++;
        }
        break;
      default:
        curPage = state.getLastPage();
        break;
    }
    state.setCurPage(curPage);
    
    Map<String, Object> map = new HashMap<>();
    map.put("startRow", state.getCurPage() * WebUtil.POST_MAX_PAGE_LINES);
    map.put("rowCount", WebUtil.POST_MAX_PAGE_LINES);
    map.put("range1", state.getRange1());
    map.put("range2", state.getRange2());
    
    List<PostView> list = postViewDao.queryByPageAndRange(map);
    return list;
  }
}