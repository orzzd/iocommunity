package com.hgd.iocommunity.service;

import com.hgd.iocommunity.dao.UserInfoMapper;
import com.hgd.iocommunity.domain.Other;
import com.hgd.iocommunity.domain.Parent;
import com.hgd.iocommunity.domain.Student;
import com.hgd.iocommunity.domain.Teacher;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class UserInfoService {
  @Resource
  private UserInfoMapper userInfoDao;
  
  public void insertNewUser(String uid, String nickname, String province, 
      String icon) {
    Map<String, Object> map = new HashMap<>();
    map.put("oid",uid);
    map.put("nickname",nickname);
    map.put("province",province);
    map.put("icon",icon);
    userInfoDao.insertNewUser(map);
  }
  
  public String usertype(String uid) {
    String type = "tourist";
    if (userInfoDao.inOtherTabel(uid) == 1) {
      type = "other";
    } else if (userInfoDao.inParentTabel(uid) == 1) {
      type = "parent";
    } else if (userInfoDao.inStudentTabel(uid) == 1) {
      type = "student";
    } else if (userInfoDao.isInstitutionTeacher(uid) == 1) {
      type = "institutionTeacher";
    } else if (userInfoDao.isSchoolTeacher(uid) == 1) {
      type = "schoolTeacher";
    }
    return type;
  }
  
  public Student getStudentInfo(String uid) {
    return  userInfoDao.queryBySid(uid);
  }
  
  public Other getOtherInfo(String uid) {
    return  userInfoDao.queryByOid(uid);
  }
  
  public Parent getParentInfo(String uid) {
    return  userInfoDao.queryByPid(uid);
  }
  
  public Teacher getTeacherInfo(String uid) {
    return  userInfoDao.queryByTid(uid);
  }
  
  public void insertTeacher(Map<String, Object> map) {
    userInfoDao.insertTeacher(map);
  }
  
  public void insertParent(Map<String, Object> map) {
    userInfoDao.insertParent(map);
  }
  
  public void insertOther(Map<String, Object> map) {
    userInfoDao.insertOther(map);
  }
  
  public void insertStudent(Map<String, Object> map) {
    userInfoDao.insertStudent(map);
  }
  
  public void deleteBySid(String uid) {
    userInfoDao.deleteBySid(uid);
  }
  
  public void deleteByTid(String uid) {
    userInfoDao.deleteByTid(uid);
  }
  
  public void deleteByOid(String uid) {
    userInfoDao.deleteByOid(uid);
  }
  
  public void deleteByPid(String uid) {
    userInfoDao.deleteByPid(uid);
  }
  
  public void deleteStudentAuthentication(String uid) {
    userInfoDao.deleteStudentAuthentication(uid);
  }
  
  public void deleteTeacherAuthentication(String uid) {
    userInfoDao.deleteTeacherAuthentication(uid);
  }
  
  public void banStudent(String uid) {
    userInfoDao.banStudent(uid);
  }
  
  public void banTeacher(String uid) {
    userInfoDao.banTeacher(uid);
  }
  
  public void banParent(String uid) {
    userInfoDao.banParent(uid);
  }
  
  public void banOther(String uid) {
    userInfoDao.banOther(uid);
  }
  
  public void addAdmin(String phonenumber,String province) {
    Map<String, Object> map = new HashMap<>();
    map.put("phonenumber",phonenumber);
    map.put("province",province);
    userInfoDao.addAdmin(map);
  }
  
  public List<String> getRangeByPhone (String phonenumber) {
    List<String> range = userInfoDao.getRangeByPhone(phonenumber);
    return range;
  }
  
  
}
