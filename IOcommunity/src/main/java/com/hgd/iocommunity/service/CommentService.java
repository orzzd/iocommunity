package com.hgd.iocommunity.service;

import com.hgd.iocommunity.dao.CommentMapper;
import com.hgd.iocommunity.dao.CommentViewMapper;
import com.hgd.iocommunity.domain.Comment;
import com.hgd.iocommunity.domain.CommentView;
import com.hgd.iocommunity.state.CommentQueryState;
import com.hgd.iocommunity.util.WebUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class CommentService {
  @Resource
  private CommentMapper commentDao;
  @Resource
  private CommentViewMapper commentViewDao;
  
  public void deleteComment(Map<String,Object> map) {
    commentDao.delete(map);
  }
  
  public int addComment(Comment comment) {
    return commentDao.insert(comment);
  }
  
  public int getMaxFloor(String postid) {
    System.out.println(commentDao.queryCountByPostid(postid));
    if (commentDao.queryCountByPostid(postid) == 0) {
      return 0;
    } else {
      return commentDao.getMaxFloor(postid);
    }
  }
  
  public int getLastPage(CommentQueryState state) {
    Map<String, Object> map = new HashMap<>();
    map.put("postid", state.getPostid());
    
    int count = commentViewDao.queryMaxCount(map);
    
    int maxPage = (count + WebUtil.COMMENT_MAX_PAGE_LINES - 1) 
        / WebUtil.COMMENT_MAX_PAGE_LINES;
    int lastPage = (maxPage > 0) ? maxPage - 1 : 0;  
    return lastPage;    
  }
  
  public List<CommentView> getCommentsByPage(
      CommentQueryState state, String page) {
    int curPage = state.getCurPage();
    switch (page) {
      case "0":
        curPage = 0;
        break;
      case "prev":
        if (curPage > 0) {
          curPage--;
        }
        break;
      case "next":
        if (curPage < state.getLastPage()) {
          curPage++;
        }
        break;
      default:
        curPage = state.getLastPage();
        break;
    }
    state.setCurPage(curPage);
    
    Map<String, Object> map = new HashMap<>();
    map.put("startRow", state.getCurPage() * WebUtil.COMMENT_MAX_PAGE_LINES);
    map.put("rowCount", WebUtil.COMMENT_MAX_PAGE_LINES);
    map.put("postid", state.getPostid());
    
    List<CommentView> list = commentViewDao.queryByPage(map);
    return list;
  }
  
  public List<CommentView> getComments(CommentQueryState state) {
    Map<String, Object> map = new HashMap<>();
    map.put("startRow", state.getCurPage() * WebUtil.COMMENT_MAX_PAGE_LINES);
    map.put("rowCount", WebUtil.COMMENT_MAX_PAGE_LINES);
    map.put("postid", state.getPostid());

    List<CommentView> list = commentViewDao.queryByPage(map);
    return list;
  }
  
  public void like(Map<String,Object> map) {
    commentDao.like(map);
  }
  
  public void unlike(Map<String,Object> map) {
    commentDao.unlike(map);
  }
  
  public int getLastPageByPostid(CommentQueryState state) {
    Map<String, Object> map = new HashMap<>();
    map.put("postid", state.getPostid());
    
    int count = commentViewDao.queryCountByPostid(map);
    
    int maxPage = (count + WebUtil.COMMENT_MAX_PAGE_LINES - 1) 
        / WebUtil.COMMENT_MAX_PAGE_LINES;
    int lastPage = (maxPage > 0) ? maxPage - 1 : 0;  
    return lastPage;    
  }
  
  public List<CommentView> getCommentsByPageAndPostid(
      CommentQueryState state, String page) {
    int curPage = state.getCurPage();
    switch (page) {
      case "0":
        curPage = 0;
        break;
      case "prev":
        if (curPage > 0) {
          curPage--;
        }
        break;
      case "next":
        if (curPage < state.getLastPage()) {
          curPage++;
        }
        break;
      default:
        curPage = state.getLastPage();
        break;
    }
    state.setCurPage(curPage);
    
    Map<String, Object> map = new HashMap<>();
    map.put("startRow", state.getCurPage() * WebUtil.COMMENT_MAX_PAGE_LINES);
    map.put("rowCount", WebUtil.COMMENT_MAX_PAGE_LINES);
    map.put("postid", state.getPostid());
    
    List<CommentView> list = commentViewDao.queryByPageAndPostid(map);
    return list;
  }
}
