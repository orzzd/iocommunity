package com.hgd.iocommunity.service;

import com.hgd.iocommunity.dao.ListOtherMapper;
import com.hgd.iocommunity.domain.Other;
import com.hgd.iocommunity.state.PageQueryState;
import com.hgd.iocommunity.util.WebUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class ListOtherService {
  @Resource
  private ListOtherMapper listOtherDao;
  
  public int getLastPage(PageQueryState pageQueryState) {
    Map<String, Object> map = new HashMap<>();
    
    int count = listOtherDao.queryMaxCount(map);
    
    int maxPage = (count + WebUtil.LIST_MAX_PAGE_LINES - 1) 
        / WebUtil.LIST_MAX_PAGE_LINES;
    int lastPage = (maxPage > 0) ? maxPage - 1 : 0;  
    return lastPage;
  }
  
  public List<Other> getOtherByPage(PageQueryState state, String page) {
    int curPage = state.getCurPage();
    switch (page) {
      case "0":
        curPage = 0;
        break;
      case "prev":
        if (curPage > 0) {
          curPage--;
        }
        break;
      case "next":
        if (curPage < state.getLastPage()) {
          curPage++;
        }
        break;
      default:
        curPage = state.getLastPage();
        break;
    }
    state.setCurPage(curPage);
    
    Map<String, Object> map = new HashMap<>();
    map.put("startRow", state.getCurPage() * WebUtil.LIST_MAX_PAGE_LINES);
    map.put("rowCount", WebUtil.LIST_MAX_PAGE_LINES);
    
    List<Other> list = listOtherDao.queryByPage(map);
    return list;
  }
  
}

