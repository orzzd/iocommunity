package com.hgd.iocommunity.service;

import com.hgd.iocommunity.dao.LikeStateMapper;
import com.hgd.iocommunity.domain.LikeState;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class LikeStateService {
  @Resource
  private LikeStateMapper likeStateDao;
  
  public void addLikeState(LikeState likeState) {
    likeStateDao.insert(likeState);
  }
  
  public LikeState selectLikeState(String postid, String uid) {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("uid", uid);
    map.put("postid", postid);
    LikeState state = likeStateDao.selectLikeState(map);
    return state;
  }
  
  public void like(Map<String, Object> map) {
    likeStateDao.like(map);
  }
  
  public void unlike(Map<String, Object> map) {
    likeStateDao.unlike(map);
  }
}
