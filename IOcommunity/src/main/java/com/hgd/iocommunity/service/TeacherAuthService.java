package com.hgd.iocommunity.service;

import com.hgd.iocommunity.dao.TeacherAuthMapper;
import com.hgd.iocommunity.domain.TeacherAuth;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service("teacherAuthService")
@Scope("singleton")
public class TeacherAuthService {
  @Resource
  private TeacherAuthMapper teacherAuthServ;
  @Resource
  private TeacherAuthMapper teacherAuthDao;
  
  public List<TeacherAuth> getAuthList() {
    List<TeacherAuth> list = teacherAuthDao.queryAll();
    return list;
  }
  
  public void deleteAuth(String picture) {
    teacherAuthDao.deleteAuth(picture);
  }

  public void deleteAuthOfTeachert(String tid) {
    teacherAuthDao.deleteAuthOfTeacher(tid);
  }
  
  public void success(String tid, String institution) {
    teacherAuthDao.deleteAuthOfTeacher(tid);
    teacherAuthDao.addPrize(institution, tid);
  }
}
