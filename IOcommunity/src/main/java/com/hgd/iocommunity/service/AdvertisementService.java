package com.hgd.iocommunity.service;

import com.hgd.iocommunity.dao.AdvertisementMapper;
import com.hgd.iocommunity.domain.Advertisement;
import com.hgd.iocommunity.state.PageQueryState;
import com.hgd.iocommunity.util.WebUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class AdvertisementService {
  @Resource
  private AdvertisementMapper advertisementDao;
  
  public int getMaxId() {
    return advertisementDao.queryMaxId();
  }
  
  public void addAdvertisement(Advertisement advertiserment) {
    advertisementDao.insert(advertiserment);
  }
  
  public void deleteAdvertisement(int adid) {
    advertisementDao.delete(adid);
  }
  
  public int getLastPage(PageQueryState pageQueryState) {
    Map<String, Object> map = new HashMap<>();
    
    int count = advertisementDao.queryMaxCount(map);
    
    int maxPage = (count + WebUtil.COMMENT_MAX_PAGE_LINES - 1) 
        / WebUtil.COMMENT_MAX_PAGE_LINES;
    int lastPage = (maxPage > 0) ? maxPage - 1 : 0;  
    return lastPage;
  }
  
  public List<Advertisement> getAdvertisementsByPage(PageQueryState state, 
      String page) {
    int curPage = state.getCurPage();
    switch (page) {
      case "0":
        curPage = 0;
        break;
      case "prev":
        if (curPage > 0) {
          curPage--;
        }
        break;
      case "next":
        if (curPage < state.getLastPage()) {
          curPage++;
        }
        break;
      default:
        curPage = state.getLastPage();
        break;
    }
    state.setCurPage(curPage);
    
    Map<String, Object> map = new HashMap<>();
    map.put("startRow", state.getCurPage() 
        * WebUtil.ADVERTISEMENT_MAX_PAGE_LINES);
    map.put("rowCount", WebUtil.ADVERTISEMENT_MAX_PAGE_LINES);
    
    List<Advertisement> list = advertisementDao.queryByPage(map);
    return list;
  }
  
  public List<Advertisement> getAllAdvertisements() {
    List<Advertisement> list = advertisementDao.queryAll();
    return list;
  }
}
