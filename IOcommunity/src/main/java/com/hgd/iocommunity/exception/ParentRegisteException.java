package com.hgd.iocommunity.exception;

@SuppressWarnings("serial")
public class ParentRegisteException extends Exception {
  public ParentRegisteException(Exception e) {
    super(e);
  }
  
  public ParentRegisteException(String msg) {
    super(msg);
  }
}
