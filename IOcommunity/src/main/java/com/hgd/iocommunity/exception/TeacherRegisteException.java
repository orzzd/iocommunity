package com.hgd.iocommunity.exception;

@SuppressWarnings("serial")
public class TeacherRegisteException extends Exception {
  public TeacherRegisteException(Exception e) {
    super(e);
  }

  public TeacherRegisteException(String msg) {
    super(msg);
  }
}
