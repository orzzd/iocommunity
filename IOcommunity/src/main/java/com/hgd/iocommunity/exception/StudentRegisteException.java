package com.hgd.iocommunity.exception;

@SuppressWarnings("serial")
public class StudentRegisteException extends Exception {
  public StudentRegisteException(Exception e) {
    super(e);
  }

  public StudentRegisteException(String msg) {
    super(msg);
  }
}
