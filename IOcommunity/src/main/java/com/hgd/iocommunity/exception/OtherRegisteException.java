package com.hgd.iocommunity.exception;

@SuppressWarnings("serial")
public class OtherRegisteException extends Exception {
  public OtherRegisteException(Exception e) {
    super(e);
  }

  public OtherRegisteException(String msg) {
    super(msg);
  }
}
