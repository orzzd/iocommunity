<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.sql.*" %>
<%
  String path = request.getContextPath();
%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>广告管理</title>

  <link href="<%=path%>/img/favicon.png" rel="icon">
  <link href="<%=path%>/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link href="<%=path%>/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<%=path%>/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<%=path%>/lib/bootstrap-fileupload/bootstrap-fileupload.css" />
  <link rel="stylesheet" type="text/css" href="<%=path%>/lib/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="<%=path%>/lib/bootstrap-daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" type="text/css" href="<%=path%>/lib/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<%=path%>/lib/bootstrap-datetimepicker/datertimepicker.css" />
  <link href="<%=path%>/css/style.css" rel="stylesheet">
  <link href="<%=path%>/css/style-responsive.css" rel="stylesheet">

</head>

<body>
  <section id="container">
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right"></div>
      </div>
      <a class="logo"><b>信奥社区</b></a>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="<%=path%>/AdminLogoutCtrl">退出</a></li>
        </ul>
      </div>
    </header>
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><img src="<%=path%>/img/friends/fr-05.jpg" class="img-circle" width="80"></p>
          
          <li class="sub-menu">
            <a href="<%=path%>/ListCountryPostsCtrl">
              <span>帖子管理</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="">
              <span>添加板块管理员</span>
            </a>
            <ul class="sub">
              <li><a href="<%=path%>/ListStudentsAdminCtrl">学生</a></li>
              <li><a href="<%=path%>/ListTeacherAdminCtrl">中小学与机构老师</a></li>
              <li><a href="<%=path%>/ListParentAdminCtrl">家长</a></li>
              <li><a href="<%=path%>/ListOtherAdminCtrl">其他</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="">
              <span>用户管理</span>
            </a>
            <ul class="sub">
              <li><a href="<%=path%>/ListStudentsCtrl">学生</a></li>
              <li><a href="<%=path%>/ListTeacherCtrl">中小学与机构老师</a></li>
              <li><a href="<%=path%>/ListParentCtrl">家长</a></li>
              <li><a href="<%=path%>/ListOtherCtrl">其他</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a class="active" href="<%=path%>/ListAdvertisementsCtrl">
              <span>广告管理</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="<%=path%>/ListStudentAuthCtrl">
              <span>学生认证</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="<%=path%>/ListTeacherAuthCtrl">
              <span>教师认证</span>
            </a>
          </li>
        </ul>
      </div>
    </aside>
    
    <section id="main-content">
      <section class="wrapper"> 
        <div class="row mt">
          <div class="col-lg-12">
            <div class="form-panel">
            <h4>添加广告</h4>
            <jsp:useBean id="advertisement" scope="request" class="com.hgd.iocommunity.domain.Advertisement" />
             <form:form id="addAdvertisement" modelAttribute="advertisement" action="AddAdvertisementCtrl" class="form-horizontal style-form" method="POST" name="myform" onsubmit="return CheckPost();">
              <div class="form-group">
                  <label class="control-label col-md-3">视频号</label>
                  <div class="col-md-3 col-xs-11">
                    <form:input path="vid" name="vid" class="form-control"/>
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-md-3">标题</label>
                  <div class="col-md-3 col-xs-11">
                    <form:input path="title" name="title" class="form-control"/>
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <button class="btn btn-theme" type="submit">提交</button>
                  </div>
                </div>
               
              </form:form>
            </div>
          </div>
        </div>
        
        <div class="row mt">
          <div class="col-md-12">
            <div class="content-panel">
              <table class="table table-striped table-advance table-hover">
                <hr>
                <thead>
                  <tr>
                    <th class="hidden-phone"><i class="fa fa-question-circle"></i> 广告编号</th>
                    <th><i class="fa fa-bookmark"></i> 视频号</th>
                    <th><i class="fa fa-bookmark"></i> 标题</th>
                    <th><i class=" fa fa-edit"></i>操作 </th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                <c:forEach var="advertisement" items="${requestScope.listAdvertisements}">
                  <tr>
                    <td>${advertisement.adid}</td>
                    <td>${advertisement.vid}</td>
                    <td>${advertisement.title}</td>
                    <td>
                      <a href="DeleteAdvertisementCtrl?adid=${advertisement.adid}">
                        <button class="btn btn-danger btn-xs">
                          <i class="fa fa-trash-o"></i>
                        </button>
                      </a>
                    </td>
                  </tr>
                </c:forEach>
                </tbody>
              </table>
              
              <table class="table table-striped table-advance table-hover">
                 <tr>
                  <td align=center>
                    <a style="color:#555" href='./ListAdvertisementsCtrl?page=0'>首 页</a>&nbsp;&nbsp;&nbsp;&nbsp;  
                    <a style="color:#555" href='./ListAdvertisementsCtrl?page=prev'>上一页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a style="color:#555" href='./ListAdvertisementsCtrl?page=next'>下一页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a style="color:#555" href='./ListAdvertisementsCtrl?page=${requestScope.lastPage}'>末 页</a>
                  </td> 
                </tr>
              </table>
            </div>
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-fileupload/bootstrap-fileupload.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script src="<%=path%>/lib/advanced-form-components.js"></script>
  
  <script type="text/javascript">
    function CheckPost()
    {
      if (myform.vid.value=="")
      {
        alert("请填写视频号");
        myform.vid.focus();
        return false;
      }
      if (myform.title.value=="")
      {
        alert("请填写标题");
        myform.title.focus();
      }
    }
  </script>
</body>
</html>
