<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.sql.*" %>
<%
  String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html dir="ltr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <title>用户管理</title>
  
  <!-- Favicons -->
  <link href="<%=path%>/img/favicon.png" rel="icon">
  <link href="<%=path%>/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="<%=path%>/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="<%=path%>/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<%=path%>/lib/bootstrap-fileupload/bootstrap-fileupload.css" />
  <link rel="stylesheet" type="text/css" href="<%=path%>/lib/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="<%=path%>/lib/bootstrap-daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" type="text/css" href="<%=path%>/lib/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<%=path%>/lib/bootstrap-datetimepicker/datertimepicker.css" />
  <!-- Custom styles for this template -->
  <link href="<%=path%>/css/style.css" rel="stylesheet">
  <link href="<%=path%>/css/style-responsive.css" rel="stylesheet">
  
  <style>
  file{
    font-size:10px;
  }
  </style>
</head>
  
<body>
  <section id="container">
  
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right"></div>
      </div>
      <!--logo start-->
      <a class="logo"><b>信奥社区</b></a>
      <!--logo end-->
      
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="<%=path%>/AdminLogoutCtrl">退出</a></li>
        </ul>
      </div>
    </header>
    
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><img src="<%=path%>/img/friends/fr-05.jpg" class="img-circle" width="80"></p>
          
          <li class="sub-menu">
            <a href="<%=path%>/ListCountryPostsCtrl">
              <span>帖子管理</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="">
              <span>添加板块管理员</span>
            </a>
            <ul class="sub">
              <li><a href="<%=path%>/ListStudentsAdminCtrl">学生</a></li>
              <li><a href="<%=path%>/ListTeacherAdminCtrl">中小学与机构老师</a></li>
              <li><a href="<%=path%>/ListParentAdminCtrl">家长</a></li>
              <li><a href="<%=path%>/ListOtherAdminCtrl">其他</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a class="active" class="active" href="">
              <span>用户管理</span>
            </a>
            <ul class="sub">
              <li><a href="<%=path%>/ListStudentsCtrl">学生</a></li>
              <li><a href="<%=path%>/ListTeacherCtrl">中小学与机构老师</a></li>
              <li><a href="<%=path%>/ListParentCtrl">家长</a></li>
              <li><a href="<%=path%>/ListOtherCtrl">其他</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="<%=path%>/ListAdvertisementsCtrl">
              <span>广告管理</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="<%=path%>/ListStudentAuthCtrl">
              <span>学生认证</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="<%=path%>/ListTeacherAuthCtrl">
              <span>教师认证</span>
            </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
        </div>
        <!-- row -->
        <div class="row mt">
          <div class="col-md-12">
            <div class="content-panel">
              <h4>学生管理</h4>
              
              <table class="table table-striped table-advance table-hover">
                <hr>
                <thead>
                  <tr>
                    
                    <th class="hidden-phone"><i class="fa fa-question-circle"></i> 用户id</th>
                    <th><i class="fa fa-bookmark"></i> 用户昵称</th>
                    <th><i class="fa fa-bookmark"></i> 用户手机号码</th>
                    <th><i class="fa fa-bookmark"></i> 用户省份</th>
                    <th><i class=" fa fa-edit"></i>操作 </th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                <c:forEach var="student" items="${requestScope.listStudents}">
                  <tr>
                    <td>${student.sid}</td>
                    <td>${student.nickname}</td>
                    <td>${student.phonenumber}</td>
                    <td>${student.province}</td>
                    <td>
                      <c:if test="${student.ban==0}">
                        <a href="<%=path%>/BanUserCtrl?uid=${student.sid}">
                          <button class="btn btn-danger btn-xs" title="禁言"><i class="fa fa-times "></i></button>
                        </a>
                      </c:if>
                      <c:if test="${student.ban==1}">
                        <a href="<%=path%>/BanUserCtrl?uid=${student.sid}">
                          <button class="btn btn-success btn-xs" title="取消禁言"><i class="fa fa-check "></i></button>
                        </a>
                      </c:if>
                    </td>
                  </tr>
                </c:forEach>
                </tbody>
              </table>
              
              <table class="table table-striped table-advance table-hover">
                 <tr>
                  <td align=center>
                    <a style="color:#555" href='./ListStudentsCtrl?page=0'>首 页</a>&nbsp;&nbsp;&nbsp;&nbsp;  
                    <a style="color:#555" href='./ListStudentsCtrl?page=prev'>上一页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a style="color:#555" href='./ListStudentsCtrl?page=next'>下一页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a style="color:#555" href='./ListStudentsCtrl?page=${requestScope.lastPage}'>末 页</a>
                  </td> 
                </tr>
              </table>
              
            </div>
            <!-- /content-panel -->
          </div>
          <!-- /col-md-12 -->
        </div>
        <!-- /row -->
      </section>
    </section>
  </section>
  
  <script src="<%=path%>/lib/jquery/jquery.min.js"></script>
  <script src="<%=path%>/lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="<%=path%>/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="<%=path%>/lib/jquery.scrollTo.min.js"></script>
  <script src="<%=path%>/lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="<%=path%>/lib/common-scripts.js"></script>
  <!--script for this page-->
  <script src="<%=path%>/lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-fileupload/bootstrap-fileupload.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script src="<%=path%>/lib/advanced-form-components.js"></script>
</body>
</html>
