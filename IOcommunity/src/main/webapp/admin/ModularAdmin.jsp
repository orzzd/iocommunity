<%@ page language="java" import="com.hgd.iocommunity.domain.PostView" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.sql.*" %>
<%
  String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html dir="ltr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <title>帖子管理</title>
  
  <!-- Favicons -->
  <link href="<%=path%>/img/favicon.png" rel="icon">
  <link href="<%=path%>/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="<%=path%>/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="<%=path%>/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<%=path%>/lib/bootstrap-fileupload/bootstrap-fileupload.css" />
  <link rel="stylesheet" type="text/css" href="<%=path%>/lib/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="<%=path%>/lib/bootstrap-daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" type="text/css" href="<%=path%>/lib/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<%=path%>/lib/bootstrap-datetimepicker/datertimepicker.css" />
  <!-- Custom styles for this template -->
  <link href="<%=path%>/css/style.css" rel="stylesheet">
  <link href="<%=path%>/css/style-responsive.css" rel="stylesheet">
  
  <style>
  file{
    font-size:10px;
  }
  </style>
</head>
  
<body>
  <section id="container">
  
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right"></div>
      </div>
      <!--logo start-->
      <a class="logo"><b>信奥社区</b></a>
      <!--logo end-->
      
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="<%=path%>/AdminLogoutCtrl">退出</a></li>
        </ul>
      </div>
    </header>
    
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><img src="<%=path%>/img/friends/fr-05.jpg" class="img-circle" width="80"></p>
         
          <li class="sub-menu">
            <a class="active" href="<%=path%>/ListCountryPostsModularCtrl">
              <span>帖子管理</span>
            </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
        </div>
        <!-- row -->
        <div class="row mt">
          <div class="col-md-12">
            <div class="content-panel">
              <table class="table table-striped table-advance table-hover">
                <h4>帖子管理</h4>
                <hr>
                <thead>
                  <tr>
                    <th class="check">批量操作</th>
                    <th class="hidden-phone"><i class="fa fa-question-circle"></i> 帖子id</th>
                    <th><i class="fa fa-bookmark"></i> 帖子标题</th>
                    <th><i class="fa fa-bookmark"></i> 发帖人id</th>
                    <th><i class="fa fa-bookmark"></i> 省份</th>
                    <th><i class=" fa fa-edit"></i>操作 </th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                <c:forEach var="postView" items="${requestScope.listCountryPosts}">
                  <tr>
                    <td><input type="checkbox" id="subcheck" name="subcheck" value="${postView.postid}"/></td>
                    <td>
                      <a href="<%=path%>/ListCommentsModularCtrl?postid=${postView.postid}">
                        ${postView.postid}
                      </a>
                    </td>
                    <td>${postView.title}</td>
                    <td>${postView.uid}</td>
                    <td>${postView.range}</td>
                    <td>
                      <c:if test="${postView.isFold==0}">
                      <a href="<%=path%>/FoldPostModularCtrl?postid=${postView.postid}">
                        <button class="btn btn-success btn-xs" title="折叠">
                          <i class="fa fa-compress"></i>
                        </button>
                      </a>
                      </c:if>
                      <c:if test="${postView.isFold==1}">
                      <a href="<%=path%>/UnfoldPostModularCtrl?postid=${postView.postid}">
                        <button class="btn btn-danger btn-xs" title="取消折叠">
                          <i class="fa fa-compress"></i>
                        </button>
                      </a>
                      </c:if>
                      <c:if test="${postView.isEssence==0}">
                      <a href="<%=path%>/DigestPostModularCtrl?postid=${postView.postid}">
                        <button class="btn btn-success btn-xs" title="加精">
                          <i class="fa fa-star"></i>
                        </button>
                      </a>
                      </c:if>
                      <c:if test="${postView.isEssence==1}">
                      <a href="<%=path%>/NodigestPostModularCtrl?postid=${postView.postid}">
                        <button class="btn btn-danger btn-xs" title="取消加精">
                          <i class="fa fa-star"></i>
                        </button>
                      </a>
                      </c:if>
                      <c:if test="${postView.commentAllowed==1}">
                      <a href="<%=path%>/CommentPostModularCtrl?postid=${postView.postid}">
                        <button class="btn btn-Success btn-xs" title="允许评论">
                          <i class="fa fa-check"></i>
                        </button>
                      </a>
                      </c:if>
                      <c:if test="${postView.commentAllowed==0}">
                      <a href="<%=path%>/NocommentPostModularCtrl?postid=${postView.postid}">
                        <button class="btn btn-danger btn-xs" title="禁止评论">
                          <i class="fa fa-times"></i>
                        </button>
                      </a>
                      </c:if>
                      <a href="<%=path%>/DeletePostModularCtrl?postid=${postView.postid}">
                        <button class="btn btn-danger btn-xs" title="删除帖子">
                          <i class="fa fa-trash-o"></i>
                        </button>
                      </a>
                    </td>
                  </tr>
                </c:forEach>
                </tbody>
              </table>
              
              <table class="table table-striped table-advance table-hover">
                 <tr>
                  <td>
                    <button onclick="batchDeletes()">批量删除</button>
                      <button onclick="batchDigest()">批量加精</button>
                      <button onclick="batchFold()">批量折叠</button>
                    </td>
                 </tr>
                 <tr>
                  <td align=center>
                    <a style="color:#555" href='./ListCountryPostsModularCtrl?page=0'>首 页</a>&nbsp;&nbsp;&nbsp;&nbsp;  
                    <a style="color:#555" href='./ListCountryPostsModularCtrl?page=prev'>上一页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a style="color:#555" href='./ListCountryPostsModularCtrl?page=next'>下一页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a style="color:#555" href='./ListCountryPostsModularCtrl?page=${requestScope.lastPage}'>末 页</a>
                  </td> 
                </tr>
              </table>
              
            </div>
            <!-- /content-panel -->
          </div>
          <!-- /col-md-12 -->
        </div>
        <!-- /row -->
      </section>
    </section>
  </section>
  
  <script src="<%=path%>/lib/jquery/jquery.min.js"></script>
  <script src="<%=path%>/lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="<%=path%>/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="<%=path%>/lib/jquery.scrollTo.min.js"></script>
  <script src="<%=path%>/lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="<%=path%>/lib/common-scripts.js"></script>
  <!--script for this page-->
  <script src="<%=path%>/lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-fileupload/bootstrap-fileupload.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script src="<%=path%>/lib/advanced-form-components.js"></script>
  <script type="text/javascript">
    function batchDeletes() {
      let checkedNum = $("input[name='subcheck']:checked").length
      if(checkedNum === 0) {
          alert("请至少选择一项!");
          return false;
      }
      if(confirm("确定删除所选项目?")) {
          let checkedList = new Array()
          $("input[name='subcheck']:checked").each(function(){
              checkedList.push($(this).val())
          })
          $.ajax({
            type: "POST",
            url: "DeletePostCtrl",
            data: {"delitems":checkedList.toString()},
            datatype: "html",
            success: function(data){
                $("[name='checkbox2']:checkbox").attr("checked",false)
                location.reload();
            },
            error:function(data){
                alert('删除失败!')
            }
          })
      }
    }
    
    function batchFold() {
      let checkedNum = $("input[name='subcheck']:checked").length
      if(checkedNum === 0) {
          alert("请至少选择一项!");
          return false;
      }
      if(confirm("确定折叠所选项目?")) {
          let checkedList = new Array()
          $("input[name='subcheck']:checked").each(function(){
              checkedList.push($(this).val())
          })
          $.ajax({
            type: "POST",
            url: "FoldPostCtrl",
            data: {"delitems":checkedList.toString()},
            datatype: "html",
            success: function(data){
                $("[name='checkbox2']:checkbox").attr("checked",false)
                location.reload();
            },
            error:function(data){
                alert('折叠失败!')
            }
          })
      }
    }
    
    function batchDigest() {
      let checkedNum = $("input[name='subcheck']:checked").length
      if(checkedNum === 0) {
          alert("请至少选择一项!");
          return false;
      }
      if(confirm("确定加精所选项目?")) {
          let checkedList = new Array()
          $("input[name='subcheck']:checked").each(function(){
              checkedList.push($(this).val())
          })
          $.ajax({
            type: "POST",
            url: "DigestPostCtrl",
            data: {"delitems":checkedList.toString()},
            datatype: "html",
            success: function(data){
                $("[name='checkbox2']:checkbox").attr("checked",false)
                location.reload();
            },
            error:function(data){
                alert('加精失败!')
            }
          })
      }
    }
  </script>
</body>
</html>
