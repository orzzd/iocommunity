<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%
  String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>信奥社区管理平台--登录</title>
  <link href="<%=path%>/img/favicon.png" rel="icon">
  <link href="<%=path%>/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link href="<%=path%>/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<%=path%>/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="<%=path%>/css/style.css" rel="stylesheet">
  <link href="<%=path%>/css/style-responsive.css" rel="stylesheet">
</head>

<body>
  <div id="login-page">
    <div class="container">
        <h2 class="form-login-heading">信奥社区管理平台</h2>
        <div class="login-wrap">
          <form:form  class="form-login" action="./AdminVerifyCodeCtrl" method="POST">
            <input style="height:37px;" type="text" class="form-control" id="userPhone"name="phonenumber" placeholder="请输入手机号" autofocus /> 
            <button style="margin-top:-55px; margin-left:235px" value="获取验证码" class="btn btn-theme" type="submit" id="getcode" >获取验证码</button>
          </form:form>
          <form:form  style="margin-top:-18px;" class="form-login" action="./AdminLoginCtrl" method="POST">
            <input type="hidden" class="form-control" id="loginUserPhone" name="phonenumber"/>
            <input type="text" name="VerifyCode" class="form-control" placeholder="请输入验证码">
            <span style="color:red;font-size:10px;text-align:center;"><i>${ sessionScope.usererror }</i></span>
            <button class="btn btn-theme btn-block" type="submit"> 登录</button>
          </form:form>
        </div>
    </div>
  </div>
  <script src="<%=path%>/lib/jquery/jquery.min.js"></script>
  <script src="<%=path%>/js/44.js"></script>
  <script src="<%=path%>/lib/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<%=path%>/lib/jquery.backstretch.min.js"></script>
  <script type="text/javascript">
    $.backstretch("<%=path%>/img/login-bg.jpg", {
      speed: 500
    });
    
    let phone = <%=session.getAttribute("loginPhonenumber")%>
    if (phone !== null) {
      $("#userPhone").val(phone)
      $("#loginUserPhone").val(phone)
    }
    
    $(function() {
        var btn = document.getElementById("getcode");
        //调用监听
        monitor($(btn));
        //点击click
        btn.onclick = function() {
            //倒计时效果  getCode回调函数  获取验证码api
            countDown($(this), getCode);
        };
        function getCode() {
                alert("验证码发送成功");
             }
    });
  </script>
</body>
</html>
