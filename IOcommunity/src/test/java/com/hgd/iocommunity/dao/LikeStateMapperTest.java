package com.hgd.iocommunity.dao;

import static org.junit.Assert.assertEquals;

import com.hgd.iocommunity.domain.LikeState;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class LikeStateMapperTest {
  @Autowired
  private LikeStateMapper likeStateDao;
  
  @Test
  @Rollback(true)
  public void testLike() {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("postid", "post2");
    map.put("uid", "2016215252");
    map.put("like", 0);
    map.put("unlike", 1);
    likeStateDao.like(map);
  }
  
  @Test
  @Rollback(true)
  public void testUnlike() {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("postid", "post2");
    map.put("uid", "2016215252");
    map.put("like", 1);
    map.put("unlike", 0);
    likeStateDao.unlike(map);
  }
}