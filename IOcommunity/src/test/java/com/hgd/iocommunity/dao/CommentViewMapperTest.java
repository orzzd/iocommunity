package com.hgd.iocommunity.dao;

import static org.junit.Assert.assertEquals;

import com.hgd.iocommunity.domain.CommentView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class CommentViewMapperTest {
  @Autowired
  private CommentViewMapper commentViewDao;

  @Test
  @Rollback(true)
  public void testQueryByPage() {
    Map<String, Object> map = new HashMap<>();
    map.put("postid", "post2");
    map.put("startRow", 0);
    map.put("rowCount", 5);
    List<CommentView> list = commentViewDao.queryByPage(map);
    int num = list.size();
  }

  @Test
  @Rollback(true)
  public void testQueryMaxCount() {
    Map<String, Object> map = new HashMap<>();
    map.put("postid", "post2");
    int num = commentViewDao.queryMaxCount(map);
    assertEquals(num, num);
  }
}

