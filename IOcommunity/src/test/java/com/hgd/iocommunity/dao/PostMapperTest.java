package com.hgd.iocommunity.dao;

import static org.junit.Assert.assertEquals;

import com.hgd.iocommunity.domain.Post;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class PostMapperTest {
  @Autowired
  private PostMapper postDao;

  @Test
  @Rollback(true)
  public void testInsert() {
    Post post = new Post("postTest", "hello", "2016215252", "安徽", 
        "圣斗士", 0, 0, 0, "2019-01-14 12:12:12", 0, 0, 0, 0);
    int num = postDao.insert(post);
    assertEquals(1, num);
  }

  @Test
  @Rollback(true)
  public void testDelete() {
    postDao.delete("postTest");
  }

  @Test
  @Rollback(true)
  public void testQueryMaxCount() {
    int num = postDao.queryMaxCount();
    assertEquals(num, num);
  }

  @Test
  @Rollback(true)
  public void testFold() {
    postDao.fold("post2");
  }

  @Test
  @Rollback(true)
  public void testUnfold() {
    postDao.unfold("post2");
  }

  @Test
  @Rollback(true)
  public void testDigest() {
    postDao.digest("post2");
  }

  @Test
  @Rollback(true)
  public void testNodigest() {
    postDao.nodigest("post2");
  }

  @Test
  @Rollback(true)
  public void testComment() {
    postDao.comment("post2");
  }
  
  @Test
  @Rollback(true)
  public void testNocomment() {
    postDao.nocomment("post2");
  }
  
  @Test
  @Rollback(true)
  public void testLike() {
    postDao.like("post2");
  }
  
  @Test
  @Rollback(true)
  public void testUnlike() {
    postDao.unlike("post2");
  }
}
