package com.hgd.iocommunity.dao;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class AuthenticationMapperTest {
  @Autowired
  private AuthenticationMapper authenticationDao;
  
  @Test
  @Rollback(true)
  public void testStudentAuthentication() {
    Map<String, Object> map = new HashMap<>();
    map.put("uid", "2016215252");
    map.put("prize", "NOIP提高组二等奖");
    map.put("picture", "sdfgfgf");
    authenticationDao.studentAuthentication(map);
  }
}
