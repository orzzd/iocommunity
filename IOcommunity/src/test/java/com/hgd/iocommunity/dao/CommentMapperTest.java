package com.hgd.iocommunity.dao;

import static org.junit.Assert.assertEquals;

import com.hgd.iocommunity.domain.Comment;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class CommentMapperTest {
  @Autowired
  private CommentMapper commentDao;
  
  @Test
  @Rollback(true)
  public void testInsert() {
    Comment comment = new Comment();
    comment.setFloor(50);
    comment.setPostid("6vQtaHpqqh3lhcQFY4NE");
    comment.setRelate(0);
    comment.setContent("自闭了");
    comment.setUid("2016215252");
    comment.setDate("2016-01-14 13:14:00");
    comment.setReply(0);
    comment.setThumbUp(0);
    comment.setThumbDown(0);
    Map<String, Object> map = new HashMap<>();
    map.put("floor", comment.getFloor());
    map.put("postid", comment.getPostid());
    commentDao.delete(map);
    int num = commentDao.insert(comment);
    assertEquals(1, num);
  }
  
  
  @Test
  @Rollback(true)
  public void testQueryCountByPostid() {
    int num = commentDao.queryCountByPostid("post2");
    assertEquals(num, num);
  }
  
  @Test
  @Rollback(true)
  public void testDelete() {
    Map<String, Object> map = new HashMap<>();
    map.put("postid", "post2");
    map.put("floor", 2);
    commentDao.delete(map);
  }
  
  @Test
  @Rollback(true)
  public void testLike() {
    Map<String, Object> map = new HashMap<>();
    map.put("floor", 2);
    map.put("postid", "post2");
    commentDao.like(map);
    assertEquals(1, 1);
  }
  
  @Test
  @Rollback(true)
  public void testUnlike() {
    Map<String, Object> map = new HashMap<>();
    map.put("floor", 2);
    map.put("postid", "post2");
    commentDao.unlike(map);
  }
}

