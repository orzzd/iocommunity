package com.hgd.iocommunity.dao;

import static org.junit.Assert.assertEquals;

import com.hgd.iocommunity.domain.CommentLikeState;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class CommentLikeStateMapperTest {
  @Autowired
  private CommentLikeStateMapper commentLikeStateDao;
  
  @Test
  @Rollback(true)
  public void testInsert() {
    CommentLikeState commentLikeState = new CommentLikeState(
        "2016215252", "post2", 2, 0, 1);
    int num = commentLikeStateDao.insert(commentLikeState);
    assertEquals(1, num);
  }
  
  @Test
  @Rollback(true)
  public void testLike() {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("floor", 2);
    map.put("postid", "post2");
    map.put("uid", "2016215252");
    map.put("like", 0);
    map.put("unlike", 1);
    commentLikeStateDao.like(map);
  }
  
  @Test
  @Rollback(true)
  public void testUnlike() {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("floor", 2);
    map.put("postid", "post2");
    map.put("uid", "2016215252");
    map.put("like", 1);
    map.put("unlike", 0);
    commentLikeStateDao.unlike(map);
  }
}

