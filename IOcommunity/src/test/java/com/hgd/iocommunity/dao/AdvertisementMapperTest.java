package com.hgd.iocommunity.dao;

import static org.junit.Assert.assertEquals;

import com.hgd.iocommunity.domain.Advertisement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class AdvertisementMapperTest {
  @Autowired
  private AdvertisementMapper advertisementDao;
  
  @Test
  @Rollback(true)
  public void testQueryByPage() {
    Map<String, Object> map = new HashMap<>();
    map.put("startRow", 0);
    map.put("rowCount", 5);
    List<Advertisement> list = advertisementDao.queryByPage(map);
    int num = list.size();
  }
  
  @Test
  @Rollback(true)
  public void testQueryMaxId() {
    int num = advertisementDao.queryMaxId();
    assertEquals(num, num);
  }
  
  @Test
  @Rollback(true)
  public void testQueryMaxCount() {
    Map<String, Object> map = new HashMap<>();
    int num = advertisementDao.queryMaxCount(map);
    assertEquals(num, num);
  }
  
  @Test
  @Rollback(true)
  public void testQueryAll() {
    List<Advertisement> list = advertisementDao.queryAll();
    int num = list.size();
    assertEquals(num, num);
  }
  
  @Test
  @Rollback(true)
  public void testInsert() {
    Advertisement advertisement = new Advertisement(9, "hi", "test");
    int num = advertisementDao.insert(advertisement);
  }
  
  @Test
  @Rollback(true)
  public void testDelete() {
    advertisementDao.delete(9);
  }
}

