package com.hgd.iocommunity.dao;

import com.hgd.iocommunity.domain.Other;
import com.hgd.iocommunity.domain.Parent;
import com.hgd.iocommunity.domain.Student;
import com.hgd.iocommunity.domain.Teacher;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class UserInfoMapperTest {
  @Autowired
  private UserInfoMapper userInfoMapper;
  
  @Test
  @Rollback(true)
  public void testInsertNewUser() {
    Map<String, Object> map = new HashMap<>();
    map.put("oid","2016215252");
    map.put("nickname","kkkk");
    map.put("province","一等奖");
    map.put("icon","asdsafasavaca");
    userInfoMapper.insertNewUser(map);
    
  }
  
  @Test
  @Rollback(true)
  public void testInOtherTabel() {
    String uid = "2016225215";
    int isInTabel = userInfoMapper.inOtherTabel(uid);
  }
  
  @Test
  @Rollback(true)
  public void testInParentTabel() {
    String uid = "2016225215";
    int isInTabel = userInfoMapper.inParentTabel(uid);
  }
  
  @Test
  @Rollback(true)
  public void testInTeacherTabel() {
    String uid = "2016225215";
    int isInTabel = userInfoMapper.inTeacherTabel(uid);
  }
  
  @Test
  @Rollback(true)
  public void testInStudentTabel() {
    String uid = "2016225215";
    int isInTabel = userInfoMapper.inStudentTabel(uid);
  }
  
  @Test
  @Rollback(true)
  public void testIsSchoolTeacher() {
    String uid = "2016225215";
    int isSchoolTeacher = userInfoMapper.isSchoolTeacher(uid);
  }
  
  @Test
  @Rollback(true)
  public void testIsInstitutionTeacher() {
    String uid = "2016225215";
    int isInstitutionTeacher = userInfoMapper.isInstitutionTeacher(uid);
  }
  
  @Test
  @Rollback(true)
  public void testQueryBySid() {
    String uid = "2016225215";
    Student student = userInfoMapper.queryBySid(uid);
  }
  
  @Test
  @Rollback(true)
  public void testQueryByOid() {
    String uid = "2016225215";
    Other other = userInfoMapper.queryByOid(uid);
  }
  
  @Test
  @Rollback(true)
  public void testQueryByTid() {
    String uid = "2016225215";
    Teacher teacher = userInfoMapper.queryByTid(uid);
  }
  
  @Test
  @Rollback(true)
  public void testQueryByPid() {
    String uid = "2016225215";
    Parent parent = userInfoMapper.queryByPid(uid);
  }
  
  @Test
  @Rollback(true)
  public void testDeleteBySid() {
    String uid = "2016225215";
    userInfoMapper.deleteBySid(uid);
  }
  
  @Test
  @Rollback(true)
  public void testDeleteByOid() {
    String uid = "2016225215";
    userInfoMapper.deleteByOid(uid);
  }
  
  @Test
  @Rollback(true)
  public void testDeleteByTid() {
    String uid = "2016225215";
    userInfoMapper.deleteByTid(uid);
  }
  
  @Test
  @Rollback(true)
  public void testDeleteByPid() {
    String uid = "2016225215";
    userInfoMapper.deleteByPid(uid);
  }
  
  @Test
  @Rollback(true)
  public void testDeleteStudentAuthentication() {
    String uid = "2016225215";
    userInfoMapper.deleteStudentAuthentication(uid);
  }
  
  @Test
  @Rollback(true)
  public void testInsertOther() {
    Map<String, Object> map = new HashMap<>();
    map.put("oid","2016215252");
    map.put("nickname","kkkk");
    map.put("province","一等奖");
    map.put("icon","asdsafasavaca");
    map.put("ban",1);
    map.put("phonenumber","12845625771");
    userInfoMapper.insertOther(map);
  }
  
  @Test
  @Rollback(true)
  public void testInsertTeacher() {
    Map<String, Object> map = new HashMap<>();
    map.put("tid","2016215252");
    map.put("nickname","kkkk");
    map.put("province","一等奖");
    map.put("icon","asdsafasavaca");
    map.put("ban",1);
    map.put("phonenumber","12845625771");
    map.put("institution","合肥工业大学");
    map.put("teachertype","1");
    userInfoMapper.insertTeacher(map);
  }
  
  @Test
  @Rollback(true)
  public void testInsertParent() {
    Map<String, Object> map = new HashMap<>();
    map.put("pid","2016215252");
    map.put("nickname","kkkk");
    map.put("province","一等奖");
    map.put("icon","asdsafasavaca");
    map.put("ban",1);
    map.put("phonenumber","12845625771");
    map.put("objective","获取信息");
    userInfoMapper.insertParent(map);
  }
  
  @Test
  @Rollback(true)
  public void testBanStudent() {
    String uid = "2016225215";
    userInfoMapper.banStudent(uid);
  }
  
  @Test
  @Rollback(true)
  public void testBanTeacher() {
    String uid = "2016225215";
    userInfoMapper.banTeacher(uid);
  }
  
  @Test
  @Rollback(true)
  public void testBanParent() {
    String uid = "2016225215";
    userInfoMapper.banParent(uid);
  }
  
  @Test
  @Rollback(true)
  public void testBanOther() {
    String uid = "2016225215";
    userInfoMapper.banOther(uid);
  }
  
  
}
