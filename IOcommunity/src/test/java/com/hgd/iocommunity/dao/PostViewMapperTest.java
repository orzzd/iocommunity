package com.hgd.iocommunity.dao;

import static org.junit.Assert.assertEquals;

import com.hgd.iocommunity.domain.PostView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class PostViewMapperTest {
  @Autowired
  private PostViewMapper postViewDao;
  
  @Test
  @Rollback(true)
  public void testQueryCount() {
    Map<String, Object> map = new HashMap<>();
    map.put("type", "热门");
    int num = postViewDao.queryCount(map);
    assertEquals(num, num);
  }
  
  @Test
  @Rollback(true)
  public void testQueryByPage() {
    Map<String, Object> map = new HashMap<>();
    map.put("type", "热门");
    map.put("sortMethod", "综合排序");
    map.put("startRow", 0);
    map.put("rowCount", 5);
    List<PostView> list = postViewDao.queryByPage(map);
    int num = list.size();
  }
  
  @Test
  @Rollback(true)
  public void testQueryOtherByPage() {
    Map<String, Object> map = new HashMap<>();
    map.put("type", "安徽");
    map.put("sortMethod", "综合排序");
    map.put("startRow", 0);
    map.put("rowCount", 5);
    List<PostView> list = postViewDao.queryOtherByPage(map);
    int num = list.size();
    assertEquals(5, num);
  }
  
}

