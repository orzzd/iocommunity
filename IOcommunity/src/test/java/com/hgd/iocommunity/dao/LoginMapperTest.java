package com.hgd.iocommunity.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class LoginMapperTest {
  @Autowired
  private LoginMapper loginMapper;
  
  @Test
  @Rollback(true)
  public void testIsModularAdmin() {
    String phonenumber = "19854123651";
    loginMapper.isModularAdmin(phonenumber);
  }
  
  @Test
  @Rollback(true)
  public void testIsSuperAdmin() {
    String phonenumber = "19854723651";
    loginMapper.isSuperAdmin(phonenumber);
  }
}
