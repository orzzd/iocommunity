package com.hgd.iocommunity.dao;

import com.hgd.iocommunity.domain.StudentAuth;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class StudentAuthMapperTest {
  @Autowired
  private StudentAuthMapper studentAuthMapper;
  
  @Test
  @Rollback(true)
  public void testQueryAll() {
    List<StudentAuth> list = studentAuthMapper.queryAll();
  }
  
  
  @Test
  @Rollback(true)
  public void testDeleteAuth() {
    String picture = "afacafarokcmas";
    studentAuthMapper.deleteAuth(picture);
  }
  
  @Test
  @Rollback(true)
  public void testDeleteAuthOfStudent() {
    String sid = "2016215244";
    studentAuthMapper.deleteAuth(sid);
  }
  
  @Test
  @Rollback(true)
  public void testAddPrize() {
    String prize = "一等奖";
    String sid = "2016215244";
    studentAuthMapper.addPrize(prize, sid);
  }
  
}

