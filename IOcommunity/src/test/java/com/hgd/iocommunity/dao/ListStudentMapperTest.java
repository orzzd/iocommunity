package com.hgd.iocommunity.dao;

import com.hgd.iocommunity.domain.Student;
import com.hgd.iocommunity.util.WebUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class ListStudentMapperTest {
  @Autowired
  private ListStudentMapper listStudentMapper;
  
  @Test
  @Rollback(true)
  public void testQueryByPage() {
    Map<String, Object> map = new HashMap<>();
    map.put("startRow", 5);
    map.put("rowCount", WebUtil.LIST_MAX_PAGE_LINES);
    List<Student> list = listStudentMapper.queryByPage(map);
    
  }
  
  @Test
  @Rollback(true)
  public void testQueryMaxCount() {
    Map<String, Object> map = new HashMap<>();
    int maxCount = listStudentMapper.queryMaxCount(map);
  }
  
}

