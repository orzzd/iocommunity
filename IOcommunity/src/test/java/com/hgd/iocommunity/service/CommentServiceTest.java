package com.hgd.iocommunity.service;

import static org.junit.Assert.assertEquals;

import com.hgd.iocommunity.domain.Comment;
import com.hgd.iocommunity.domain.CommentView;
import com.hgd.iocommunity.service.CommentService;
import com.hgd.iocommunity.state.CommentQueryState;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class CommentServiceTest {
  @Autowired
  private CommentService commentService;
  
  @Test
  @Rollback(true)
  public void testDelete() {
    Map<String, Object> map = new HashMap<>();
    map.put("postid", "post2");
    map.put("floor", 2);
    commentService.deleteComment(map);
  }
  
  
  @Test
  @Rollback(true)
  public void testGetMaxFloor() {
    String postid = "post1";
    int num = commentService.getMaxFloor(postid);
    assertEquals(num, num);
  }
  
  @Test
  @Rollback(true)
  public void testGetLastPage() {
    CommentQueryState state = new CommentQueryState(1, "post1");
    int num = commentService.getLastPage(state);
    assertEquals(num, num);
  }
  
  @Test
  @Rollback(true)
  public void testGetCommentsByPage() {
    CommentQueryState state = new CommentQueryState(1, "post2");
    String page = "next";
    List<CommentView> list = commentService.getCommentsByPage(state, page);
  }
  
  @Test
  @Rollback(true)
  public void testGetComments() {
    CommentQueryState state = new CommentQueryState(1, "post2");
    List<CommentView> list = commentService.getComments(state);
  }
  
  @Test
  @Rollback(true)
  public void testLike() {
    Map<String, Object> map = new HashMap<>();
    map.put("floor", 2);
    map.put("postid", "post2");
    commentService.like(map);
  }
  
  @Test
  @Rollback(true)
  public void testUnlike() {
    Map<String, Object> map = new HashMap<>();
    map.put("floor", 2);
    map.put("postid", "post2");
    commentService.unlike(map);
  }
}
