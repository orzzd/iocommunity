package com.hgd.iocommunity.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class AuthenticationServiceTest {
  @Autowired
  private AuthenticationService authenticationSvc;
  
  @Test
  @Rollback(true)
  public void testStudentAuthentication() {
    String uid = "owpeu4mfNEz24Ic9s1EqoZJiUhNE";
    String prize = "全国一等奖";
    String path = "asasfavsafaagagsd";
    authenticationSvc.studentAuthentication(uid, prize, path);
  }
  
  @Test
  @Rollback(true)
  public void testTeacherAuthentication() {
    String uid = "owpeu4sWyl5VkuVOX1h0M48qxh8A";
    String institution = "合肥工业大学";
    String path = "asasfavsafaagagsd";
    authenticationSvc.teacherAuthentication(uid, institution, path);
  }
}