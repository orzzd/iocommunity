package com.hgd.iocommunity.service;

import static org.junit.Assert.assertEquals;

import com.hgd.iocommunity.domain.Other;
import com.hgd.iocommunity.domain.Parent;
import com.hgd.iocommunity.domain.Student;
import com.hgd.iocommunity.domain.Teacher;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class UserInfoServiceTest {
  @Autowired
  private UserInfoService userInfoSvc;
  
  @Test
  @Rollback(true)
  public void testUsertype() {
    String uid = "1a21sa54fasd1aa";
    String type = userInfoSvc.usertype(uid);
    assertEquals("tourist", type);
  } 
  
  @Test
  @Rollback(true)
  public void testGetStudentInfo() {
    String uid = "owpeu4gWyuq9kvEkw4mN7Y6Wlmu0";
    Student student = userInfoSvc.getStudentInfo(uid);
  }
  
  @Test
  @Rollback(true)
  public void testGetOtherInfo() {
    String uid = "owpeu4rCrDR2tigGCYnCjLD1GOe4";
    Other other = userInfoSvc.getOtherInfo(uid);
  }
  
 
  public void testDeleteBySid() {
    String uid = "afaxafavasfvax";
    userInfoSvc.deleteBySid(uid);
  }
  
  public void testDeleteByTid() {
    String uid = "afsscaasdasf";
    userInfoSvc.deleteByTid(uid);
  }
  
  public void testDeleteByOid() {
    String uid = "afaxvaasfasfvax";
    userInfoSvc.deleteByOid(uid);
  }
  
  public void testDeleteByPid() {
    String uid = "afaxvasfvax";
    userInfoSvc.deleteByPid(uid);
  }
  
  public void testDeleteStudentAuthentication() {
    String uid = "afaxvasffczvvax";
    userInfoSvc.deleteStudentAuthentication(uid);
  }
  
  public void testDeleteTeacherAuthentication() {
    String uid = "afaxvasafavczxfvax";
    userInfoSvc.deleteTeacherAuthentication(uid);
  }
  
  public void testBanStudent() {
    String uid = "aaasfvzxafsv";
    userInfoSvc.banStudent(uid);
  }
  
  public void testBanTeacher() {
    String uid = "igmghmngf";
    userInfoSvc.banTeacher(uid);
  }
  
  public void testBanParent() {
    String uid = "vccdf";
    userInfoSvc.banParent(uid);
  }
  
  public void testBanOther() {
    String uid = "wefdvv";
    userInfoSvc.banOther(uid);
  }

  public void addAdmin() {
    String phonenumber = "18788810170";
    String province = "安徽";
    userInfoSvc.addAdmin(phonenumber, province);
  }
}