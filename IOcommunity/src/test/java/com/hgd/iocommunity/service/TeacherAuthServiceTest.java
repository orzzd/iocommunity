package com.hgd.iocommunity.service;

import com.hgd.iocommunity.domain.TeacherAuth;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class TeacherAuthServiceTest {
  @Autowired
  private TeacherAuthService teacherAuthService;
  
  @Test
  @Rollback(true)
  public void testDeleteAuth() {
    String picture = "542156512.jpg";
    teacherAuthService.deleteAuth(picture);
  }
  
  @Test
  @Rollback(true)
  public void testDeleteAuthOfStudent() {
    String tid = "w13124215";
    teacherAuthService.deleteAuthOfTeachert(tid);
  }
  
  @Test
  @Rollback(true)
  public void testSuccess() {
    String tid = "w13124215";
    String institution = "全国一等奖";
    teacherAuthService.success(institution, tid);
  }
}
