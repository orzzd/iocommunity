package com.hgd.iocommunity.service;

import static org.junit.Assert.assertEquals;

import com.hgd.iocommunity.domain.Teacher;
import com.hgd.iocommunity.state.PageQueryState;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class ListTeacherServiceTest {
  @Autowired
  private ListTeacherService listTeacherSvc;
  
  @Test
  @Rollback(true)
  public void testGetLastPage() {
    PageQueryState state = new PageQueryState(1);
    int num = listTeacherSvc.getLastPage(state);
  }
  
  @Test
  @Rollback(true)
  public void testGetTeacherByPage() {
    PageQueryState state = new PageQueryState(1);
    String page = "next";
    List<Teacher> list = listTeacherSvc.getTeacherByPage(state, "next");
  }
}
