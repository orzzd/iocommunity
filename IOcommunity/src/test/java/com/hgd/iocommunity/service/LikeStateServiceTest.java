package com.hgd.iocommunity.service;

import com.hgd.iocommunity.domain.LikeState;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class LikeStateServiceTest {
  @Autowired
  private LikeStateService likeStateSvc;
  
  @Test
  @Rollback(true)
  public void testSelectLikeState() {
    String postid = "123512";
    String uid = "2016215252";
    likeStateSvc.selectLikeState(postid, uid);
  }
  
  @Test
  @Rollback(true)
  public void testLike() {
    Map<String, Object> map = new HashMap<String, Object>();
    String postid = "123512";
    String uid = "2016215252";
    int like = 20;
    map.put("uid", uid);
    map.put("postid", postid);
    map.put("like",like);
    likeStateSvc.like(map);
  }
  
  @Test
  @Rollback(true)
  public void testUnlike() {
    Map<String, Object> map = new HashMap<String, Object>();
    String postid = "123512";
    String uid = "2016215252";
    int unlike = 20;
    map.put("uid", uid);
    map.put("postid", postid);
    map.put("unlike",unlike);
    likeStateSvc.like(map);
  }
}
