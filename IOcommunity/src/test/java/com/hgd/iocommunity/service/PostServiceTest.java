package com.hgd.iocommunity.service;

import static org.junit.Assert.assertEquals;

import com.hgd.iocommunity.domain.Post;
import com.hgd.iocommunity.domain.PostView;
import com.hgd.iocommunity.state.PostQueryState;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class PostServiceTest {
  @Autowired
  private PostService postService;
  
  @Test
  @Rollback(true)
  public void testAddPost() {
    Post post = new Post("post11", "hello", "2016215252", "安徽", 
        "圣斗士", 0, 0, 0, "2019-01-14 12:12:12", 0, 0, 0, 0);
    int num = postService.addPost(post);
    assertEquals(1, num);
  }
    
  @Test
  @Rollback(true)
  public void testDeletePost() {
    postService.deletePost("post2");
  }
  
  @Test
  @Rollback(true)
  public void testGetMaxCount() {
    int num = postService.getMaxCount();
    assertEquals(num, num);
  }
  
  @Test
  @Rollback(true)
  public void testGetLastPage() {
    PostQueryState state = new PostQueryState(0, "全国", "综合排序");
    int num = postService.getLastPage(state);
    assertEquals(num, num);
  }
  
  @Test
  @Rollback(true)
  public void testGetPostsByPage() {
    PostQueryState state = new PostQueryState(0, "全国", "综合排序");
    String page = "next";
    List<PostView> list = postService.getPostsByPage(state, page);
    assertEquals(5, list.size());
  }
  
  @Test
  @Rollback(true)
  public void testGetPosts() {
    PostQueryState state = new PostQueryState(0, "全国", "综合排序");
    List<PostView> list = postService.getPosts(state);
    assertEquals(5, list.size());
  }
  
  @Test
  @Rollback(true)
  public void testFold() {
    postService.fold("post1");
  }
  
  @Test
  @Rollback(true)
  public void testUnfold() {
    postService.unfold("post1");
  }
  
  @Test
  @Rollback(true)
  public void testDigest() {
    postService.digest("post1");
  }
  
  @Test
  @Rollback(true)
  public void testNodigest() {
    postService.nodigest("post1");
  }
  
  @Test
  @Rollback(true)
  public void testComment() {
    postService.comment("post1");
  }
  
  @Test
  @Rollback(true)
  public void testNocomment() {
    postService.nocomment("post1");
  }
  
  @Test
  @Rollback(true)
  public void testLike() {
    postService.like("post1");
  }
  
  @Test
  @Rollback(true)
  public void testUnlike() {
    postService.unlike("post1");
  }
} 
