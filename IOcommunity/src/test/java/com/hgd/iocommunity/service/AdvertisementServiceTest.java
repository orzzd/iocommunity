package com.hgd.iocommunity.service;

import com.hgd.iocommunity.domain.Advertisement;
import com.hgd.iocommunity.state.PageQueryState;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/ApplicationContext.xml", 
    "classpath:/SpringDispatcher-servlet.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", 
    defaultRollback = true)//必须有 否则不能实现数据回滚
@Transactional//必须有 否则不能实现数据回滚

public class AdvertisementServiceTest {
  @Autowired
  private AdvertisementService advertisementService;
  
  @Test
  @Rollback(true)
  public void testAddAdvertisement() {
    Advertisement advertiserment = new Advertisement(8, "gfdgdf", "test");
    advertisementService.addAdvertisement(advertiserment);
  }
  
  @Test
  @Rollback(true)
  public void testDeleteAdvertisement() {
    int adid = 8;
    advertisementService.deleteAdvertisement(adid);
  }
  
  @Test
  @Rollback(true)
  public void testGetLastPage() {
    PageQueryState state = new PageQueryState(0);
    int num = advertisementService.getLastPage(state);
    System.out.println(num);
  }
  
  @Test
  @Rollback(true)
  public void testGetAdvertisementsByPage() {
    PageQueryState state = new PageQueryState(0);
    String page = "next";
    List<Advertisement> list = advertisementService
        .getAdvertisementsByPage(state, page);
    for (int i = 0; i < list.size(); i++) {
      Advertisement advertisement = list.get(i);
      System.out.println(advertisement.getAdid());
      System.out.println(advertisement.getVid());
      System.out.println(advertisement.getTitle());
    }
  }
  
  @Test
  @Rollback(true)
  public void testGetAllAdvertisements() {
    List<Advertisement> list = advertisementService.getAllAdvertisements();
    for (int i = 0; i < list.size(); i++) {
      Advertisement advertisement = list.get(i);
      System.out.println(advertisement.getAdid());
      System.out.println(advertisement.getVid());
      System.out.println(advertisement.getTitle());
    }
  }
}
